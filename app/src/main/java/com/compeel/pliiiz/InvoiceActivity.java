package com.compeel.pliiiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.model.Conversation;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.DialogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.realm.Realm;

public class InvoiceActivity extends AppCompatActivity {


    private LinearLayout ll_invoice;
    private ProgressDialog mProgress;

    private Button terminate;
    private AppCompatImageView add_product;
    private TextView request;

    private JSONObject jsonObject;
    JSONObject element;
    private String pliii_id;
    private String line;
    private Double total_amount;
    JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // To make activity full screen.
        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_add_pliii_invoice);

        request = (TextView) findViewById(R.id.request);
        getExtrasData();
        setButtonAction();


    }


    private void setButtonAction() {
        terminate = (Button) findViewById(R.id.terminate);
        add_product = (AppCompatImageView) findViewById(R.id.add_product);
        ll_invoice = (LinearLayout) findViewById(R.id.ll_invoice);
        add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Create LinearLayout

                LinearLayout parent = new LinearLayout(InvoiceActivity.this);

                parent.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                parent.setOrientation(LinearLayout.HORIZONTAL);
                parent.setWeightSum(2);

                final LinearLayout.LayoutParams lparams = new TableLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
                lparams.weight = 1;

                final EditText product = new EditText(InvoiceActivity.this);
                product.setLayoutParams(lparams);
                product.setHint(getString(R.string.libelle));

                parent.addView(product);

                final EditText price = new EditText(InvoiceActivity.this);
                price.setLayoutParams(lparams);
                price.setInputType(InputType.TYPE_CLASS_NUMBER);
                price.setHint(getString(R.string.montant));

                parent.addView(price);

                ll_invoice.addView(parent);

            }
        });


        terminate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                jsonObject = new JSONObject();
                line = "";
                total_amount = 0.0;
                try {
                    jsonObject.put("pliii_id", pliii_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray = new JSONArray();
                for (int i = 0; i < ll_invoice.getChildCount(); i++) {
                    LinearLayout layout = (LinearLayout) ll_invoice.getChildAt(i);

                    EditText product = (EditText) layout.getChildAt(0);
                    EditText price = (EditText) layout.getChildAt(1);
                    element = new JSONObject();

                    try {


                        if (TextUtils.isEmpty(product.getText().toString()) || TextUtils.isEmpty(product.getText().toString())) {

                        } else {
                            element.put("product", product.getText().toString());
                            element.put("price", price.getText().toString());
                            jsonArray.put(element);
                            line += product.getText().toString() + " : " + price.getText().toString() + "\n";
                            total_amount += Double.parseDouble(price.getText().toString());
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


                try {
                    jsonObject.put("invoice", (Object) jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                line += "TOTAL : " + total_amount;

                if (jsonArray.length() ==  0) {
                    Intent intent = new Intent();
                    intent.putExtra("invoice", "");
                    setResult(RESULT_OK, intent);
                    finish();
                } else
                    sendInvoiceToCustomer();

            }
        });
    }


    private void getExtrasData() {
        pliii_id = getIntent().getStringExtra("pliii_id");
        request.setText(getIntent().getStringExtra("request"));
    }


    private void sendInvoiceToCustomer() {


        final String url = Config.BASE_URL + "run_invoice/";
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();

        System.out.println(jsonObject.toString());


        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Veuillez patienter pendant que Pliiiz vérifie votre numero de téléphone ...");
        mProgress.show();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                System.out.println(" RESPONSE " + response.toString());

                try {
                    String status = response.getString("success");


                    if (status.equals("false")) {

                        //Une erreur est survenue
                        mProgress.dismiss();
                        Snackbar.make(ll_invoice, getString(R.string.pliiizer_unknown_error), Snackbar.LENGTH_LONG).show();
                        Intent intent = new Intent();
                        intent.putExtra("invoice", "");
                        setResult(RESULT_OK, intent);
                        finish();

                    } else {
                        // Create invoice out conversation
                        mProgress.dismiss();

                        //get Pliii
                        Pliii pliii = DataBaseHelper.getPliiiInfoByID(pliii_id);

                        Realm realm = PliiizApplication.getRealmInstance();
                        realm.beginTransaction();
                        Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
                        conversation.setDate("" + new Date().getTime());
                        conversation.setType(Conversation.TEXT);
                        conversation.setDirection(Conversation.OUT);
                        conversation.setContent(line);
                        conversation.setPliii(pliii);
                        realm.commitTransaction();


                        Intent intent = new Intent();
                        intent.putExtra("invoice", line);
                        setResult(RESULT_OK, intent);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgress.dismiss();

                DialogUtils.showConnectivityErrorDialog(InvoiceActivity.this,R.drawable.add_photo
                        ,getString(R.string.no_connection),getString(R.string.no_connection_long));
            }
        }) {


        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);
    }
}
