package com.compeel.pliiiz.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.compeel.pliiiz.PliiiActivity;
import com.compeel.pliiiz.PliiiEndDetailActivity;
import com.compeel.pliiiz.PliiizerArrivedDetailActivity;
import com.compeel.pliiiz.R;
import com.compeel.pliiiz.holder.PliiiHolder;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.AccountUtils;


import java.util.ArrayList;

public class PliiiAdapter extends RecyclerView.Adapter<PliiiHolder> {

    ArrayList<Pliii> list;
    Context context;

    //ajouter un constructeur prenant en entrée une liste
    public PliiiAdapter(Context context, ArrayList<Pliii> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public PliiiHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pliii, viewGroup, false);

        return new PliiiHolder(context, view);

    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    @Override
    public void onBindViewHolder(final PliiiHolder pliiiHolder, int position) {
        final Pliii myObject = list.get(position);

        pliiiHolder.bind(myObject);
        pliiiHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPliii(myObject);
            }
        });


    }


    private void onClickPliii(Pliii pliii) {

        AccountUtils.setCurrentPliii(context,pliii.getPliii_id());

        if (pliii.getStatus() == 0) {
            Intent mIntent = new Intent(context, PliiiActivity.class);
            Bundle mBundle = new Bundle();

            mBundle.putString("pliii_date", pliii.getDate());
            mBundle.putString("pliiizer_name", pliii.getPliiizer_name());
            mBundle.putString("pliii_request", pliii.getRequest());
            mBundle.putString("pliiizer_lon", pliii.getPliiizer_lon());
            mBundle.putString("pliiizer_lat", pliii.getPliiizer_lat());
            mBundle.putString("pliiizer_lat", pliii.getPliiizer_lat());
            mBundle.putString("pliiizer_avatar", pliii.getPliiizer_avatar());
            mBundle.putString("pliii_id", pliii.getPliii_id());

            mBundle.putString("customer_id", pliii.getCustomer_id());
            mBundle.putString("customer_name", pliii.getCustomer_name());
            mBundle.putString("customer_avatar", pliii.getCustomer_avatar());

            if (pliii.isPliiizer_arrived()) {
                mIntent = new Intent(context, PliiizerArrivedDetailActivity.class);
                mBundle.putString("pliii_id", pliii.getPliii_id());
                mIntent.putExtras(mBundle);
                context.startActivity(mIntent);
            } else {
                mIntent.putExtras(mBundle);
                if (Build.VERSION.SDK_INT >= 11) {
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                } else {
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                }
                context.startActivity(mIntent);
            }

        } else {
            Intent mIntent = new Intent(context, PliiiEndDetailActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putString("pliii_id", pliii.getPliii_id());
            mIntent.putExtras(mBundle);
            context.startActivity(mIntent);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}