package com.compeel.pliiiz.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.compeel.pliiiz.R;
import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.helper.AdapterRequestHelper;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.helper.PlaySoundHelper;
import com.compeel.pliiiz.model.Conversation;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.widget.CircularImageView;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;


public class ConversationAdapter extends RecyclerView.Adapter {


    ArrayList<Conversation> dataItems;
    private Context context;

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private static final int VIEW_TYPE_MESSAGE_INVOICE_SENT = 3;
    private static final int VIEW_TYPE_MESSAGE_INVOICE_RECEIVED = 4;
    private static final int VIEW_TYPE_MESSAGE_NEW_PLIII_RECEIVED = 5;
    private static final int VIEW_TYPE_MESSAGE_INVOICE_CONFIRMED_RECEIVED = 6;
    private static final int VIEW_TYPE_MESSAGE_NEW_PLIII__AUDIO_RECEIVED = 7;


    public ConversationAdapter(Context context, ArrayList<Conversation> dataItems) {
        this.context = context;
        this.dataItems = dataItems;


    }


    private void refreshList() {
        Realm realm = PliiizApplication.getRealmInstance();
        realm.beginTransaction();
        RealmResults<Conversation> allSorted = realm.where(Conversation.class).findAllSorted("id");
        Conversation conversation = allSorted.last();
        realm.commitTransaction();

        dataItems.add(conversation);
        notifyDataSetChanged();

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }


    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        Conversation message = dataItems.get(position);

        if (message.getDirection() == Conversation.IN) {
            // If the current user is the sender of the message
            if (message.getType() == Conversation.INVOICE)
                return VIEW_TYPE_MESSAGE_INVOICE_RECEIVED;
            else if (message.getType() == Conversation.RCV_NEW_PLIII)
                return VIEW_TYPE_MESSAGE_NEW_PLIII_RECEIVED;
            else if (message.getType() == Conversation.RCV_NEW_PLIII_AUDIO)
                return VIEW_TYPE_MESSAGE_NEW_PLIII__AUDIO_RECEIVED;

            else if (message.getType() == Conversation.RCV_CONFIRMED_INVOICE)
                return VIEW_TYPE_MESSAGE_INVOICE_CONFIRMED_RECEIVED;
            else
                return VIEW_TYPE_MESSAGE_RECEIVED;
        } else {
            // If some other user sent the message
            if (message.getType() == Conversation.INVOICE)
                return VIEW_TYPE_MESSAGE_INVOICE_SENT;

            else
                return VIEW_TYPE_MESSAGE_SENT;
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        System.out.println("viewType " + viewType);
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_chat_message_item_out, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_chat_message_item_in, parent, false);
            return new ReceivedMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_INVOICE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_chat_message_item_invoice_out, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_INVOICE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_chat_message_item_invoice_in, parent, false);
            return new ReceivedInvoiceMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_NEW_PLIII_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_chat_message_item_new_pliii_in, parent, false);
            return new ReceivedNewPliiiMessageHolder(view);

        } else if (viewType == VIEW_TYPE_MESSAGE_NEW_PLIII__AUDIO_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_chat_message_item_new_audio_pliii_in, parent, false);
            return new ReceivedNewPliiiAudioMessageHolder(view);

        } else if (viewType == VIEW_TYPE_MESSAGE_INVOICE_CONFIRMED_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_chat_message_item_customer_confirmation_in, parent, false);
            return new ReceivedConfirmedInvoiceMessageHolder(view);
        }


        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Conversation message = dataItems.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
                break;

            case VIEW_TYPE_MESSAGE_INVOICE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_INVOICE_RECEIVED:
                ((ReceivedInvoiceMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_NEW_PLIII_RECEIVED:
                ((ReceivedNewPliiiMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_INVOICE_CONFIRMED_RECEIVED:
                ((ReceivedConfirmedInvoiceMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_NEW_PLIII__AUDIO_RECEIVED:
                ((ReceivedNewPliiiAudioMessageHolder) holder).bind(message);
                break;

        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText;
        RelativeTimeTextView timeText;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (RelativeTimeTextView) itemView.findViewById(R.id.text_message_time);
        }

        void bind(Conversation message) {
            messageText.setText(message.getContent());

            // Format the stored timestamp into a readable String using method.
            timeText.setReferenceTime(Long.parseLong(message.getDate()));
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, nameText;
        RelativeTimeTextView timeText;
        CircularImageView profileImage;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (RelativeTimeTextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            profileImage = (CircularImageView) itemView.findViewById(R.id.image_message_profile);
        }

        void bind(Conversation message) {
            messageText.setText(message.getContent());

            // Format the stored timestamp into a readable String using method.
            timeText.setReferenceTime(Long.parseLong(message.getDate()));

            nameText.setText(message.getPliii().getPliiizer_name());

            // Insert the profile image from the URL into the ImageView.
            //Utils.displayRoundImageFromUrl(mContext, message.getSender().getProfileUrl(), profileImage);

            if (AccountUtils.isPliiizer(context)) {
                CommonUtils.loadImage(message.getPliii().getPliiizer_avatar(), profileImage);
            } else {

                CommonUtils.loadImage(message.getPliii().getCustomer_avatar(), profileImage);
            }

        }


    }


    private class ReceivedInvoiceMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, nameText;
        RelativeTimeTextView timeText;
        CircularImageView profileImage;
        Button confirm_invoice;

        ReceivedInvoiceMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (RelativeTimeTextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            profileImage = (CircularImageView) itemView.findViewById(R.id.image_message_profile);
            confirm_invoice = (Button) itemView.findViewById(R.id.confirm_invoice);
        }

        void bind(final Conversation message) {
            String line = "";
            try {
                JSONArray jsonArray = new JSONArray(message.getContent());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                    String price = jsonObject.getString("price");
                    String product = jsonObject.getString("product");
                    line += product + " : " + price + "\n";
                }
                line += "\n";
                line += Html.fromHtml("<b>" + "TOTAL : " + message.getPliii().getPliii_total_amount() + "</b>") + "\n";
            } catch (JSONException e) {
                e.printStackTrace();
            }

            messageText.setText(line);

            // Format the stored timestamp into a readable String using method.
            timeText.setReferenceTime(Long.parseLong(message.getDate()));

            nameText.setText(message.getPliii().getPliiizer_name());

            // Insert the profile image from the URL into the ImageView.
            //Utils.displayRoundImageFromUrl(mContext, message.getSender().getProfileUrl(), profileImage);

            CommonUtils.loadImage(message.getPliii().getPliiizer_avatar(), profileImage);

            confirm_invoice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AdapterRequestHelper.confirmPliii(context, message);
                    DataBaseHelper.confirmPliii(message.getPliii().getPliii_id());

                    refreshList();

                    //Create text conversation to answer the customer


                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        confirm_invoice.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_gray_bg));
                    } else {
                        confirm_invoice.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_gray_bg));
                    }
                    confirm_invoice.setTextColor(context.getResources().getColor(R.color.colorWhite));
                    confirm_invoice.setEnabled(false);
                    confirm_invoice.setText(context.getString(R.string.confirme));
                }
            });


            if (message.getPliii().isPliii_confirmed() == true) {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    confirm_invoice.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_gray_bg));
                } else {
                    confirm_invoice.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_gray_bg));
                }
                confirm_invoice.setTextColor(context.getResources().getColor(R.color.colorWhite));
                confirm_invoice.setEnabled(false);
                confirm_invoice.setText(context.getString(R.string.confirme));

            } else {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    confirm_invoice.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_primary_bg));
                } else {
                    confirm_invoice.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_primary_bg));
                }

                confirm_invoice.setTextColor(context.getResources().getColor(R.color.colorWhite));
            }
        }
    }


    private class ReceivedNewPliiiMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, nameText;
        RelativeTimeTextView timeText;
        CircularImageView profileImage;
        Button accept_pliii;

        ReceivedNewPliiiMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (RelativeTimeTextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            profileImage = (CircularImageView) itemView.findViewById(R.id.image_message_profile);
            accept_pliii = (Button) itemView.findViewById(R.id.accept_pliii);
        }

        void bind(final Conversation message) {
            messageText.setText(context.getString(R.string.hello_i_want) + " " + message.getPliii().getRequest() + ". Merci.");

            // Format the stored timestamp into a readable String using method.
            timeText.setReferenceTime(Long.parseLong(message.getDate()));

            nameText.setText(message.getPliii().getCustomer_name());

            if (message.getPliii().isPliiizer_accept() == true) {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_white_bg));
                } else {
                    accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_white_bg));
                }
                accept_pliii.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                accept_pliii.setEnabled(false);
                accept_pliii.setText(context.getString(R.string.pliii_accepted));

            } else {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_primary_bg));
                } else {
                    accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_primary_bg));
                }

                accept_pliii.setTextColor(context.getResources().getColor(R.color.colorWhite));
            }


            CommonUtils.loadImage(message.getPliii().getCustomer_avatar(), profileImage);

            accept_pliii.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AdapterRequestHelper.acceptPliii(context, message);
                    DataBaseHelper.acceptPliii(message.getPliii().getPliii_id());

                    Realm realm = PliiizApplication.getRealmInstance();
                    realm.beginTransaction();
                    Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
                    conversation.setDate("" + new Date().getTime());
                    conversation.setType(Conversation.TEXT);
                    conversation.setDirection(Conversation.OUT);
                    conversation.setContent(message.getContent());
                    conversation.setPliii(message.getPliii());
                    realm.commitTransaction();

                    DataBaseHelper.acceptPliii(message.getPliii().getPliii_id());
                    refreshList();

                    //Set Current Pliii
                    AccountUtils.setCurrentPliii(context, message.getPliii().getPliii_id());

                    //Create text conversation to answer the customer


                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_white_bg));
                    } else {
                        accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_white_bg));
                    }
                    accept_pliii.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                    accept_pliii.setEnabled(false);
                    accept_pliii.setText(context.getString(R.string.pliii_accepted));
                }
            });
        }
    }


    private class ReceivedConfirmedInvoiceMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, nameText;
        RelativeTimeTextView timeText;
        CircularImageView profileImage;

        ReceivedConfirmedInvoiceMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (RelativeTimeTextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            profileImage = (CircularImageView) itemView.findViewById(R.id.image_message_profile);
        }

        void bind(final Conversation message) {

            // Format the stored timestamp into a readable String using method.
            timeText.setReferenceTime(Long.parseLong(message.getDate()));

            nameText.setText(message.getPliii().getCustomer_name());


            CommonUtils.loadImage(message.getPliii().getCustomer_avatar(), profileImage);

        }
    }


    private class ReceivedNewPliiiAudioMessageHolder extends RecyclerView.ViewHolder {
        TextView nameText;
        SeekBar messageText;
        RelativeTimeTextView timeText;
        CircularImageView profileImage;
        AppCompatImageView media_control;
        SeekBar text_message_body;
        Button accept_pliii;

        ReceivedNewPliiiAudioMessageHolder(View itemView) {
            super(itemView);

            messageText = (SeekBar) itemView.findViewById(R.id.text_message_body);
            timeText = (RelativeTimeTextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            profileImage = (CircularImageView) itemView.findViewById(R.id.image_message_profile);
            media_control = (AppCompatImageView) itemView.findViewById(R.id.media_control);
            text_message_body = (SeekBar) itemView.findViewById(R.id.text_message_body);
            accept_pliii = (Button) itemView.findViewById(R.id.accept_pliii);

        }

        void bind(final Conversation message) {


            // Format the stored timestamp into a readable String using method.
            timeText.setReferenceTime(Long.parseLong(message.getDate()));

            nameText.setText(message.getPliii().getCustomer_name());


            CommonUtils.loadImage(message.getPliii().getCustomer_avatar(), profileImage);

            //Play Audio onclick
            media_control.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaySoundHelper helper = new PlaySoundHelper(context, message.getPliii().getRequest(), text_message_body, media_control);
                    helper.play();
                }
            });


            if (message.getPliii().isPliiizer_accept() == true) {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_white_bg));
                } else {
                    accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_white_bg));
                }
                accept_pliii.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                accept_pliii.setEnabled(false);
                accept_pliii.setText(context.getString(R.string.pliii_accepted));

            } else {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_primary_bg));
                } else {
                    accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_primary_bg));
                }

                accept_pliii.setTextColor(context.getResources().getColor(R.color.colorWhite));
            }

            accept_pliii.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AdapterRequestHelper.acceptPliii(context, message);
                    System.out.println("PRE " + message.getContent());

                    Realm realm = PliiizApplication.getRealmInstance();
                    realm.beginTransaction();
                    Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
                    conversation.setDate("" + new Date().getTime());
                    conversation.setType(Conversation.TEXT);
                    conversation.setDirection(Conversation.OUT);
                    conversation.setContent(message.getContent());
                    conversation.setPliii(message.getPliii());
                    realm.commitTransaction();

                    DataBaseHelper.acceptPliii(message.getPliii().getPliii_id());
                    refreshList();

                    //Create text conversation to answer the customer


                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_white_bg));
                    } else {
                        accept_pliii.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_white_bg));
                    }
                    accept_pliii.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                    accept_pliii.setEnabled(false);
                    accept_pliii.setText(context.getString(R.string.pliii_accepted));
                }
            });
        }


    }


}

