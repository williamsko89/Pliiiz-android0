package com.compeel.pliiiz.config;

/**
 * Created by William on 12/09/2017.
 */

public class Config {

    public static String API_USER_BASE_URL = "http://pliiizchat.compeel.com/user/";
    public static String BASE_URL = "http://pliiizchat.compeel.com/api/v0/";
    public static String MQTT_BASE_URL = "broker.hivemq.com";
    public static String API_PLIII_BASE_URL = "http://pliiizchat.compeel.com/pliii/";
    public static String THUMBOR_SERVER_URL = "http://52.14.211.4:8888/unsafe/50x50/";


    public static String ACTION_NO_TRACKING = "0";
    public static String ACTION_START_TRACKING = "1";
    public static String ACTION_START_STOP_TRACKING = "-1";
    public static String ACTION_STOP_TRACKING = "-2";

}
