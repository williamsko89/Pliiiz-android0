package com.compeel.pliiiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.compeel.pliiiz.adapter.ViewPagerAdapter;
import com.compeel.pliiiz.utilities.AccountUtils;

public class WelcomeActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private ViewPager intro_images;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;

    private ViewPagerAdapter mAdapter;

    private int[] mImageResources = {
            R.mipmap.pager_img_1,
            R.mipmap.welcome_img_2,
            R.mipmap.welcome_img_3,

    };

    private int[] mText = {
            R.string.no_lose_time,
            R.string.suivre_coure,
            R.string.payez_livraison,

    };

    private Button register, connect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome);
        setReference();
        setButtonAction();


    }


    private void setButtonAction() {

        register = (Button) findViewById(R.id.register);
        connect = (Button) findViewById(R.id.connect);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AccountUtils.isFirstRun(WelcomeActivity.this))
                    startActivity(new Intent(WelcomeActivity.this, RegistrationActivity.class));
                else {

                    if (AccountUtils.isPliiizer(WelcomeActivity.this)) {
                        startActivity(new Intent(WelcomeActivity.this, PliiizerPliiiListActivity.class));
                    } else

                        startActivity(new Intent(WelcomeActivity.this, RegistrationActivity.class));
                }

            }
        });

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AccountUtils.isFirstRun(WelcomeActivity.this))
                    startActivity(new Intent(WelcomeActivity.this, ConnectionActivity.class));
                else {
                    if (AccountUtils.isPliiizer(WelcomeActivity.this)) {
                        startActivity(new Intent(WelcomeActivity.this, PliiizerPliiiListActivity.class));
                    } else
                        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                }

            }
        });
    }

    public void setReference() {

        intro_images = (ViewPager) this.findViewById(R.id.pager_introduction);
        pager_indicator = (LinearLayout) this.findViewById(R.id.viewPagerCountDots);

        mAdapter = new ViewPagerAdapter(WelcomeActivity.this, mImageResources, mText);
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        intro_images.setOnPageChangeListener(this);
        setUiPageViewController();
    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }


    @Override
    public void onClick(View v) {


    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}
