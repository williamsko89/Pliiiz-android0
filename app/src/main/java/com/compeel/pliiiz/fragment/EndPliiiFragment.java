package com.compeel.pliiiz.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.compeel.pliiiz.R;
import com.compeel.pliiiz.adapter.PliiiAdapter;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.model.Pliii;

import java.util.ArrayList;

public class EndPliiiFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    PliiiAdapter pliiiAdapter;
    ArrayList pliiis;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_list_pliii, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);


        listView = (RecyclerView) rootView.findViewById(R.id.list_course);
        listView.setLayoutManager(new LinearLayoutManager(getActivity()));




        loadAllPliii();


        return rootView;

    }


    private void loadAllPliii() {

        pliiis = DataBaseHelper.getEndedPliiiList();
        pliiiAdapter = new PliiiAdapter(getActivity(), pliiis);


        pliiiAdapter.notifyDataSetChanged();
        listView.setAdapter(pliiiAdapter);


    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    private void refreshList() {
        //do processing to get new data and set your listview's adapter, maybe  reinitialise the loaders you may be using or so
        //when your data has finished loading, cset the refresh state of the view to false
        loadAllPliii();
        swipeRefreshLayout.setRefreshing(false);

    }
}
