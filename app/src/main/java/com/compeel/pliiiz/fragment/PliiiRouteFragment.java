package com.compeel.pliiiz.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadrosid.lib.drawroutemap.DrawMarker;
import com.ahmadrosid.lib.drawroutemap.DrawRouteMaps;
import com.compeel.pliiiz.R;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


public class PliiiRouteFragment extends Fragment implements
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback {

    private GoogleMap mMap;


    private String mPliiizer_name;
    private String mPliiizer_lon;
    private String mPliiizer_lat;
    private String mPliii_id;
    private Pliii mPliii;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_route_pliii, null, false);


        super.onCreate(savedInstanceState);


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        return rootView;
    }


    public static PliiiRouteFragment newInstance(String pliii_id) {

        PliiiRouteFragment pliiiRouteFragment = new PliiiRouteFragment();
        Bundle b = new Bundle();
        b.putString("pliii_id", pliii_id);
        pliiiRouteFragment.setArguments(b);

        return pliiiRouteFragment;


    }

    private void getExtrasArguments() {

        mPliii_id = getArguments().getString("pliii_id");
        System.out.println(" mPliii_id " + mPliii_id);
        mPliii = DataBaseHelper.getPliiiInfoByID(mPliii_id);

        mPliiizer_name = mPliii.getPliiizer_name();
        mPliiizer_lon = mPliii.getPliiizer_lon();
        mPliiizer_lat = mPliii.getPliiizer_lat();


    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        getExtrasArguments();

        mMap = googleMap;
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        if (mPliii != null) {

            LatLng origin = null;
            LatLng destination = null;


            if (AccountUtils.isPliiizer(getActivity())) {
                origin = new LatLng(Double.parseDouble(mPliiizer_lat), Double.parseDouble(mPliiizer_lon));
                destination = new LatLng(Double.parseDouble(mPliii.getCustomer_lat()), Double.parseDouble(mPliii.getCustomer_lon()));
            } else {
                destination = new LatLng(Double.parseDouble(mPliiizer_lat), Double.parseDouble(mPliiizer_lon));
                origin = new LatLng(Double.parseDouble(AccountUtils.getUserLat(getActivity())), Double.parseDouble(AccountUtils.getUserLon(getActivity())));
            }


            DrawRouteMaps.getInstance(getActivity())
                    .draw(origin, destination, mMap);

            if (AccountUtils.isPliiizer(getActivity())) {
                //DrawMarker.getInstance(getActivity()).draw(mMap, origin, R.drawable.location_primary, mPliii.getCustomer_name());
                //DrawMarker.getInstance(getActivity()).draw(mMap, destination, R.drawable.location, getString(R.string.me));
            } else {
                //DrawMarker.getInstance(getActivity()).draw(mMap, origin, R.drawable.location, getString(R.string.me));
                //DrawMarker.getInstance(getActivity()).draw(mMap, destination, R.drawable.location_primary, mPliiizer_name);
            }

            loadImage(getActivity(), "http://52.14.211.4:8888/unsafe/50x50/http://www.waterfalls.hamilton.ca/images/Waterfall_Collage_home_sm1.jpg",origin);
            loadImage(getActivity(),"http://52.14.211.4:8888/unsafe/50x50/http://www.waterfalls.hamilton.ca/images/Waterfall_Collage_home_sm1.jpg",destination);
            System.out.println(Config.THUMBOR_SERVER_URL+mPliii.getPliiizer_avatar());


            LatLngBounds bounds = new LatLngBounds.Builder()
                    .include(origin)
                    .include(destination).build();
            Point displaySize = new Point();
            getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 100, 10));

        }


    }

    void loadImage(Context context, String url, final LatLng latLng) {


        Target mTarget = new Target() {
            @Override
            public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                //Do something
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                        // Specifies the anchor to be at a particular point in the marker image.
                        .anchor(0.5f, 1));



            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.with(context)
                .load(url)
                .into(mTarget);
    }
}
