package com.compeel.pliiiz.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.compeel.pliiiz.PliiiActivity;
import com.compeel.pliiiz.R;
import com.compeel.pliiiz.adapter.ConversationAdapter;
import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.model.Conversation;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.service.MQTTService;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;


public class ConversationFragment extends Fragment implements PliiiActivity.Updateable {


    private EditText request;
    private AppCompatImageView send;



    private RecyclerView conversationRecyclerView;
    private ConversationAdapter conversationAdapter;
    private ArrayList conversations;


    private String pliii_id;
    private Pliii pliii;

    private View rootView;


    public static ConversationFragment newInstance(String pliii_id) {

        ConversationFragment pliiiRouteFragment = new ConversationFragment();
        Bundle b = new Bundle();
        b.putString("pliii_id", pliii_id);
        pliiiRouteFragment.setArguments(b);

        return pliiiRouteFragment;


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        rootView = inflater.inflate(R.layout.activity_chat_message_view, container, false);

        conversations = new ArrayList();
        conversationRecyclerView = (RecyclerView) rootView.findViewById(R.id.chat_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        conversationRecyclerView.setLayoutManager(layoutManager);

        setAnimEditText();
        loadConversations();


        return rootView;
    }


    private void setAnimEditText() {
        request = (EditText) rootView.findViewById(R.id.request);


        send = (AppCompatImageView) rootView.findViewById(R.id.sendRquest);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray, getActivity().getTheme()));
        } else {
            send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray));
        }

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("msg_type", "9");
                    jsonObject.put("from", AccountUtils.getUserId(getActivity()));
                    jsonObject.put("to", pliii.getPliiizer_id());
                    jsonObject.put("message", request.getText().toString());
                    jsonObject.put("created_at", "" + new Date().getTime());
                    jsonObject.put("pliii_id", pliii.getPliii_id());
                    jsonObject.put("to_name", pliii.getPliiizer_name());
                    jsonObject.put("from_name", AccountUtils.getAccountName(getActivity()));
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                //Async Task to
                createSimpleTextMessage(jsonObject);

            }
        });

        request.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray, getActivity().getTheme()));
                } else {
                    send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray));
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if(s.length() > 0){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.success_gray, getActivity().getTheme()));
                    } else {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.success_gray));
                    }
                }

                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray, getActivity().getTheme()));
                    } else {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray));
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() > 0){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.success_gray, getActivity().getTheme()));
                    } else {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.success_gray));
                    }
                }

                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray, getActivity().getTheme()));
                    } else {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray));
                    }
                }
            }


        });
    }


    private void loadConversations() {

        pliii_id = getArguments().getString("pliii_id");


        conversations = DataBaseHelper.getConversationListByPliiiID(pliii_id);
        pliii = DataBaseHelper.getPliiiInfoByID(pliii_id);


        conversationAdapter = new ConversationAdapter(getActivity(), conversations);
        conversationAdapter.notifyDataSetChanged();
        conversationRecyclerView.setAdapter(conversationAdapter);
    }

    private void getLastConversation() {
        Realm realm = PliiizApplication.getRealmInstance();
        realm.beginTransaction();
        RealmResults<Conversation> allSorted = realm.where(Conversation.class).findAllSorted("id");
        Conversation conversation = allSorted.last();
        realm.commitTransaction();

        conversations.add(conversation);
        conversationAdapter.notifyDataSetChanged();
        conversationRecyclerView.scrollToPosition(conversationAdapter.getItemCount()-1);
    }


    private void createSimpleTextMessage(JSONObject jsonObject) {
        /*{"msg_type":"10","from":"612287","to":"gYQKtiIH","message":"oui oui",
                "created_at":9499494949,"pliiiz_id":"980098428",
                "to_name":"hello world hello world","from_name":"William de SOUZA"}

                */

        String channel = "";
        if (AccountUtils.isPliiizer(getActivity()))
            channel = pliii.getCustomer_id();
        else channel = pliii.getPliiizer_id();

        Intent i = new Intent(getActivity(), MQTTService.class);
        Bundle b = new Bundle();
        b.putString("channel", channel);
        b.putString("message", new String(jsonObject.toString()));
        i.setAction(CommonUtils.MQTT_PUBLISH_ACTION);
        i.putExtras(b);
        getActivity().startService(i);


        //(PliiiActivity) (getActivity()).mqttService.publishTextOnChannel(pliii.getPliiizer_id(), new String(jsonObject.toString()));

        //Record message into database

        //Record first conversation
        Realm realm = PliiizApplication.getRealmInstance();
        realm.beginTransaction();

        Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
        conversation.setDate("" + new Date().getTime());
        conversation.setType(Conversation.TEXT);
        conversation.setDirection(Conversation.OUT);
        conversation.setContent(request.getText().toString());
        conversation.setPliii(pliii);
        realm.commitTransaction();

        //Empty edittext
        emptyMessageZone();

        //update list
        getLastConversation();


    }

    private void emptyMessageZone() {
        request.setText("");
    }


    @Override
    public void update() {
        getLastConversation();
    }
}
