package com.compeel.pliiiz;


import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.widget.CircularImageView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;


public class PliiizerArrivedEndActivity extends AppCompatActivity implements
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback {

    private GoogleMap mMap;
    private Pliii mPliii;
    private Toolbar toolbar;

    private TextView pliiizer_name;
    private CircularImageView profile_image;

    private Button button_redo_pliii;
    private String mPliiizer_name;
    private String pliii_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_detail_pliii_pliiizer_arrived_end);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pliiizer_name = (TextView) findViewById(R.id.pliiizer_name);
        profile_image = (CircularImageView) findViewById(R.id.profile_image);

        button_redo_pliii = (Button) findViewById(R.id.button_redo_pliii);


        setPliiiInfo();


    }

    private void getExtrasArguments() {

        pliii_id = getIntent().getStringExtra("pliii_id");
        mPliii = DataBaseHelper.getPliiiInfoByID(pliii_id);
        mPliiizer_name = mPliii.getPliiizer_name();


    }

    private void setPliiiInfo() {
        getExtrasArguments();
        pliiizer_name.setText(mPliiizer_name);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        CommonUtils.loadImage(mPliii.getPliiizer_avatar(), profile_image);


        button_redo_pliii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //FinshRun API
                DataBaseHelper.updatePliiiStatus(pliii_id, 1);

                startActivity(new Intent(PliiizerArrivedEndActivity.this, MainActivity.class));
            }
        });

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (mPliii != null) {

            Point displaySize = new Point();
            getWindowManager().getDefaultDisplay().getSize(displaySize);
            //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 200, 15));


        }


    }


}
