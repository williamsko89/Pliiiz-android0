package com.compeel.pliiiz;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.helper.VolleyRequestHelper;
import com.compeel.pliiiz.http.VolleyMultipartRequest;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.service.MQTTService;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.DialogUtils;
import com.dewarder.holdinglibrary.HoldingButtonLayout;
import com.dewarder.holdinglibrary.HoldingButtonLayoutListener;
import com.transitionseverywhere.Scene;
import com.transitionseverywhere.Transition;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class MainActivity extends AppCompatActivity implements HoldingButtonLayoutListener {


    private MediaRecorder recorder = null;
    private int output_formats[] = {MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP};
    private String fileName;

    private static final DateFormat mFormatter = new SimpleDateFormat("mm:ss:SS");
    private static final float SLIDE_TO_CANCEL_ALPHA_MULTIPLIER = 2.5f;
    private static final long TIME_INVALIDATION_FREQUENCY = 50L;

    private HoldingButtonLayout mHoldingButtonLayout;
    private TextView mTime;
    private View mSlideToCancel;
    private LinearLayout pendulum;

    private int mAnimationDuration;
    private ViewPropertyAnimator mTimeAnimator;
    private ViewPropertyAnimator mSlideToCancelAnimator;
    private ViewPropertyAnimator mInputAnimator;

    private long mStartTime;
    private Runnable mTimerRunnable;

    private MQTTService mqttService;

    private EditText request;
    private AppCompatImageView pliiizers;
    private AppCompatImageView send;
    Transition transitionMgr;

    public static ProgressDialog mProgress;

    private TextView hello;

    private boolean bounded;

    private Scene mNormalNewRequestScene;
    private Scene mAnimatedRequestScene;
    private Stack<Scene> mSceneStack;

    private ViewGroup mRootScene;

    private boolean is_audio_request = true;

    private Animation mAnimation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);


        Intent intent = new Intent(CommonUtils.START_MQTT_SERVICE_ACTION);
        intent.putExtra("action", "start");
        sendBroadcast(intent);


        loadContentView();
        setAnimEditText();
    }


    private void loadContentView() {

        if (mProgress != null && mProgress.isShowing())
            mProgress.dismiss();
        setContentView(R.layout.activity_main_scene1);

        //mRootScene = (ViewGroup)findViewById(R.id.transitions_container);


        pliiizers = (AppCompatImageView) findViewById(R.id.pliiizers);
        pliiizers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PliiiListActivity.class));
            }
        });

        String username = AccountUtils.getAccountName(this);
        hello = (TextView) findViewById(R.id.hello);
        hello.setText("Hello " + username);

        //mNormalNewRequestScene = Scene.getSceneForLayout(mRootScene,R.layout.activity_main_scene1,MainActivity.this);
        //mAnimatedRequestScene = Scene.getSceneForLayout(mRootScene,R.layout.activity_main_scene2,MainActivity.this);

        mHoldingButtonLayout = (HoldingButtonLayout) findViewById(R.id.input_holder);
        mHoldingButtonLayout.addListener(this);

        mTime = (TextView) findViewById(R.id.time);
        mSlideToCancel = findViewById(R.id.slide_to_cancel);


        mAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mAnimation = AnimationUtils.loadAnimation(this, R.anim.anim);
        pendulum = (LinearLayout)findViewById(R.id.pendulum);
        pendulum.startAnimation(mAnimation);


    }

    private void setAnimEditText() {
        request = (EditText) findViewById(R.id.request);
        send = (AppCompatImageView) findViewById(R.id.sendRquest);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray, getTheme()));
        } else {
            send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray));
        }

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Scene scene = Scene.getSceneForLayout(transitionsContainer,R.layout.activity_main_scene2,MainActivity.this);
                //TransitionManager.go(scene);
            }
        });

        request.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                is_audio_request = false;
                mHoldingButtonLayout.setButtonEnabled(false);
                mHoldingButtonLayout.setAnimateHoldingView(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray, getTheme()));
                } else {
                    send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray));
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (s.length() > 0) {
                    is_audio_request = false;
                    mHoldingButtonLayout.setButtonEnabled(false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.success_black_gray, getTheme()));
                    } else {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.success_black_gray));
                    }
                } else {
                    is_audio_request = true;
                    mHoldingButtonLayout.setButtonEnabled(true);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray, getTheme()));
                    } else {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray));
                    }


                }

                System.out.println("is_audio_request " + is_audio_request);

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    is_audio_request = false;
                    mHoldingButtonLayout.setButtonEnabled(false);
                    mHoldingButtonLayout.setAnimateHoldingView(false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.success_black_gray, getTheme()));
                    } else {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.success_black_gray));
                    }
                } else {
                    is_audio_request = true;
                    mHoldingButtonLayout.setButtonEnabled(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray, getTheme()));
                    } else {
                        send.setImageDrawable(getResources().getDrawable(R.drawable.microphone_gray));
                    }
                }
            }


        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mProgress = new ProgressDialog(MainActivity.this);
                mProgress.setMessage("Veuillez patienter ...");
                mProgress.setCancelable(false);
                mProgress.show();


                sendRequest();
            }
        });
    }


    public void sendRequest() {


        final String url = Config.API_PLIII_BASE_URL + "new_run/";
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();


        StringRequest req = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        System.out.println(" RESPONSE " + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("success");


                            if (status.equals("false")) {
                                //Une erreur est survenue
                                if (mProgress != null)
                                    mProgress.dismiss();
                                DialogUtils.showConnectivityErrorDialog(MainActivity.this, R.drawable.add_photo
                                        , getString(R.string.phone_validation_error), getString(R.string.sorry_no_pliiizer));

                                Toast.makeText(MainActivity.this, getString(R.string.sorry_no_pliiizer), Toast.LENGTH_LONG).show();

                            } else {
                                //On ne fait rien on attend réponse MQTT


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(" error " + error.toString());


                error.printStackTrace();


                mProgress.dismiss();

                DialogUtils.showConnectivityErrorDialog(MainActivity.this, R.drawable.add_photo
                        , getString(R.string.no_connection), getString(R.string.no_connection_long));
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("request", request.getText().toString());
                params.put("uuid", CommonUtils.getDeviceID(MainActivity.this));
                params.put("lon", AccountUtils.getUserLon(MainActivity.this));
                params.put("lat", AccountUtils.getUserLat(MainActivity.this));

                return params;
            }


        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);
    }


    public void sendAudioRequest() {

        mProgress = new ProgressDialog(MainActivity.this);
        mProgress.setMessage("Veuillez patienter ...");
        mProgress.show();


        final String url = Config.API_PLIII_BASE_URL + "new_run/";
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(com.android.volley.Request.Method.POST, url, new Response.Listener<NetworkResponse>() {

            @Override
            public void onResponse(NetworkResponse response) {
                mProgress.dismiss();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(new String(error.toString()));
                DialogUtils.showConnectivityErrorDialog(MainActivity.this, R.drawable.add_photo
                        , getString(R.string.no_connection), getString(R.string.no_connection_long));

            }
        }) {
            //pass String parameters here
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("request", request.getText().toString());
                params.put("uuid", CommonUtils.getDeviceID(MainActivity.this));
                params.put("lon", AccountUtils.getUserLon(MainActivity.this));
                params.put("lat", AccountUtils.getUserLat(MainActivity.this));
                return params;
            }

            //pass header
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/multipart");
                return headers;
            }

            //pass file here (*/* - means you can pass any kind of file)
            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> up_params = new HashMap<>();
                up_params.put("audio", new DataPart(fileName, VolleyRequestHelper.getByteFromFilePath(CommonUtils.getFileAbsolutePathForFile(fileName)), "*/*"));
                return up_params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(multipartRequest);

    }

    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bounded = true;
            MQTTService.PliiizBinder pliiizBinder = (MQTTService.PliiizBinder) service;
            mqttService = pliiizBinder.getMqttServiceInstance();



            System.out.println("AccountUtils.getUserId(MainActivity.this) "+AccountUtils.getUserId(MainActivity.this));
            mqttService.subscribeTo(AccountUtils.getUserId(MainActivity.this));

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bounded = false;
            mqttService = null;
        }
    };


    @Override
    protected void onStart() {
        super.onStart();

        Intent mIntent = new Intent(this, MQTTService.class);
        bindService(mIntent, mServiceConnection, BIND_AUTO_CREATE);

        CommonUtils.startOnceTrackingService();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bounded) {
            unbindService(mServiceConnection);
        }

        CommonUtils.stopTrackingService();
    }


    public void startPliii(String request) {
        mProgress = new ProgressDialog(MainActivity.this);
        mProgress.setMessage("Veuillez patienter ...");
        mProgress.show();

        sendRequest();
    }


    @Override
    protected void onResume() {

        super.onResume();
        CommonUtils.startOnceTrackingService();
    }

    @Override
    protected void onPause() {

        super.onPause();
        CommonUtils.stopTrackingService();
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
        CommonUtils.stopTrackingService();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        System.exit(0);
    }

    @Override
    public void onBeforeExpand() {
        System.out.println("is_audio_request " + is_audio_request);
        if (is_audio_request == true) {

            System.out.println("is_audio_request " + is_audio_request);
            cancelAllAnimations();

            mSlideToCancel.setTranslationX(0f);
            mSlideToCancel.setAlpha(0f);
            mSlideToCancel.setVisibility(View.VISIBLE);

            mSlideToCancelAnimator = mSlideToCancel.animate().alpha(1f).setDuration(mAnimationDuration);
            mSlideToCancelAnimator.start();

            mInputAnimator = request.animate().alpha(0f).setDuration(mAnimationDuration);
            mInputAnimator.setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    request.setVisibility(View.INVISIBLE);
                    mInputAnimator.setListener(null);
                }
            });
            mInputAnimator.start();
            startRecording();

            mTime.setTranslationY(mTime.getHeight());
            mTime.setAlpha(0f);
            mTime.setVisibility(View.VISIBLE);
            mTimeAnimator = mTime.animate().translationY(0f).alpha(1f).setDuration(mAnimationDuration);
            mTimeAnimator.start();
        } else {
            sendRequest();
        }

    }

    @Override
    public void onExpand() {
        mStartTime = System.currentTimeMillis();
        invalidateTimer();
    }

    @Override
    public void onBeforeCollapse() {
        if (is_audio_request == true) {
            cancelAllAnimations();

            mSlideToCancelAnimator = mSlideToCancel.animate().alpha(0f).setDuration(mAnimationDuration);
            mSlideToCancelAnimator.setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSlideToCancel.setVisibility(View.INVISIBLE);
                    mSlideToCancelAnimator.setListener(null);
                }
            });
            mSlideToCancelAnimator.start();

            request.setAlpha(0f);
            request.setVisibility(View.VISIBLE);
            mInputAnimator = request.animate().alpha(1f).setDuration(mAnimationDuration);
            mInputAnimator.start();

            mTimeAnimator = mTime.animate().translationY(mTime.getHeight()).alpha(0f).setDuration(mAnimationDuration);
            mTimeAnimator.setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mTime.setVisibility(View.INVISIBLE);
                    mTimeAnimator.setListener(null);
                }
            });
            mTimeAnimator.start();
        }

    }

    @Override
    public void onCollapse(boolean isCancel) {
        if (is_audio_request) {
            stopTimer();
            if (isCancel) {
                Toast.makeText(this, "Action canceled! Time " + getFormattedTime(), Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "Action submitted! Time ", Toast.LENGTH_SHORT).show();

            }
            stopRecording();

            if (isCancel)
                CommonUtils.deleteFile(fileName);
            else
                sendAudioRequest();
            is_audio_request = true;
        }


    }

    @Override
    public void onOffsetChanged(float offset, boolean isCancel) {
        if (is_audio_request) {



            mSlideToCancel.setTranslationX(-mHoldingButtonLayout.getWidth() * offset);
            mSlideToCancel.setAlpha(1 - SLIDE_TO_CANCEL_ALPHA_MULTIPLIER * offset);
        }

    }

    private void invalidateTimer() {
        mTimerRunnable = new Runnable() {
            @Override
            public void run() {
                mTime.setText(getFormattedTime());
                invalidateTimer();
            }
        };

        mTime.postDelayed(mTimerRunnable, TIME_INVALIDATION_FREQUENCY);
    }

    private void stopTimer() {
        if (mTimerRunnable != null) {
            mTime.getHandler().removeCallbacks(mTimerRunnable);
        }
    }

    private void cancelAllAnimations() {
        if (mInputAnimator != null) {
            mInputAnimator.cancel();
        }

        if (mSlideToCancelAnimator != null) {
            mSlideToCancelAnimator.cancel();
        }

        if (mTimeAnimator != null) {
            mTimeAnimator.cancel();
        }
    }

    private String getFormattedTime() {
        return mFormatter.format(new Date(System.currentTimeMillis() - mStartTime));
    }


    private void startRecording() {
        if (is_audio_request) {
            fileName = CommonUtils.getFileName();
            recorder = new MediaRecorder();

            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(output_formats[CommonUtils.currentFormat]);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setOutputFile(CommonUtils.getFileAbsolutePathForFile(fileName));

            recorder.setOnErrorListener(errorListener);
            recorder.setOnInfoListener(infoListener);

            try {
                recorder.prepare();
                recorder.start();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void stopRecording() {


        if (is_audio_request) {
            if (null != recorder) {

                try {
                    recorder.stop();
                    recorder.reset();
                    recorder.release();

                    recorder = null;
                } catch (Exception e) {

                }

            }
        }

    }


    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
        }
    };


}