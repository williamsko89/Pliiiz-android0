package com.compeel.pliiiz;


import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.DialogUtils;
import com.compeel.pliiiz.widget.CircularImageView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PliiizerArrivedDetailActivity extends AppCompatActivity implements
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback {

    private GoogleMap mMap;
    private Pliii mPliii;
    private Toolbar toolbar;

    private TextView pliiizer_name;
    private CircularImageView profile_image;
    private TextView pliiizer_note_information, pliii_invoice, pliii_total_price;
    private AppCompatRatingBar rating;

    private Button button_redo_pliii;
    private String mPliiizer_name;
    private String pliii_id;

    private float rate = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_detail_pliii_pliiizer_arrived);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pliiizer_name = (TextView) findViewById(R.id.pliiizer_name);
        profile_image = (CircularImageView) findViewById(R.id.profile_image);
        pliiizer_note_information = (TextView) findViewById(R.id.pliiizer_note_information);
        pliii_invoice = (TextView) findViewById(R.id.pliii_invoice);
        pliii_total_price = (TextView) findViewById(R.id.pliii_total_price);

        rating = (AppCompatRatingBar) findViewById(R.id.rating);

        button_redo_pliii = (Button) findViewById(R.id.button_redo_pliii);


        setPliiiInfo();


    }

    private void getExtrasArguments() {

        pliii_id = getIntent().getStringExtra("pliii_id");
        mPliii = DataBaseHelper.getPliiiInfoByID(pliii_id);
        mPliiizer_name = mPliii.getPliiizer_name();


    }

    private void setPliiiInfo() {
        getExtrasArguments();
        pliiizer_name.setText(mPliiizer_name);
        pliiizer_note_information.setText(getString(R.string.pliiizer_note_information) + " " + mPliiizer_name);
        pliii_invoice.setText(getPliiiInvoice(mPliii.getPliii_invoice()));
        pliii_total_price.setText(Html.fromHtml("<b>" + "TOTAL A PAYER : " + mPliii.getPliii_total_amount() + "</b>") + "\n");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        CommonUtils.loadImage(mPliii.getPliiizer_avatar(), profile_image);


        button_redo_pliii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //FinshRun API
                ratePliii();
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    button_redo_pliii.setBackgroundDrawable(ContextCompat.getDrawable(PliiizerArrivedDetailActivity.this, R.drawable.button_white_bg));
                } else {
                    button_redo_pliii.setBackgroundDrawable(ContextCompat.getDrawable(PliiizerArrivedDetailActivity.this, R.drawable.button_white_bg));
                }
                button_redo_pliii.setTextColor(PliiizerArrivedDetailActivity.this.getResources().getColor(R.color.colorPrimaryDark));
                button_redo_pliii.setEnabled(false);

                //Update pliii status in database

                DataBaseHelper.updatePliiiStatus(pliii_id, 1);

                Bundle b = new Bundle();
                b.putString("pliii_id", pliii_id);
                Intent intent = new Intent(PliiizerArrivedDetailActivity.this, PliiizerArrivedEndActivity.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rate = rating;
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (mPliii != null) {

            Point displaySize = new Point();
            getWindowManager().getDefaultDisplay().getSize(displaySize);
            //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 200, 15));


        }


    }

    private String getPliiiInvoice(String invoice) {
        String line = "";
        try {
            JSONArray jsonArray = new JSONArray(invoice);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String price = jsonObject.getString("price");
                String product = jsonObject.getString("product");
                line += product + " : " + price + "\n";
            }
            line += "\n";
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        } catch (Exception a) {
            return "";
        }
        return line;
    }


    private void ratePliii() {
        final String url = Config.BASE_URL + "finish_run/?format=json&pliii_id=" + pliii_id + "&rate=" + rate;
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();


        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        DataBaseHelper.setPliiiRate(pliii_id, (int) rate);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogUtils.showConnectivityErrorDialog(PliiizerArrivedDetailActivity.this, R.drawable.add_photo
                        , getString(R.string.no_connection), getString(R.string.no_connection_long));
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

}
