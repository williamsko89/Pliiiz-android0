package com.compeel.pliiiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.DialogUtils;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;


public class ConnectionActivity extends AppCompatActivity {

    private Button button_connect;
    private JSONObject jsonBody;
    private ProgressDialog mProgress;
    private EditText phone;
    private CountryCodePicker ccp;


    private static final int REQUEST_LOCATION_PERMISSION = 1600;


    @Override
    public void onStart() {

        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {

        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);


        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_LOCATION_PERMISSION);

        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.setCountryForNameCode(CommonUtils.getCountryCode(this));

        jsonBody = new JSONObject();


        loadContentView();


    }

    private void loadContentView() {

        phone = (EditText) findViewById(R.id.phone);
        button_connect = (Button) findViewById(R.id.button_connect);
        button_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidConnectionForm(phone))
                    getBody();
                else ;
            }
        });
    }


    private boolean isValidConnectionForm(
            EditText signUpPhone) {
        boolean valid = true;
        View focus = null;
        String phone = signUpPhone.getText().toString();


        if (TextUtils.isEmpty(phone)) {
            valid = false;
            signUpPhone.setError(getString(R.string.required_phone));
            focus = signUpPhone;
        }


        if (!valid) {
            focus.requestFocus();
        }

        return valid;
    }

    private void getBody() {


        try {

            jsonBody = new JSONObject();
            jsonBody.put("uuid", CommonUtils.getDeviceID(this));
            jsonBody.put("phone", phone.getText().toString());
            jsonBody.put("lon", Double.parseDouble(AccountUtils.getUserLon(this)));
            jsonBody.put("lat", Double.parseDouble(AccountUtils.getUserLat(this)));

            AccountUtils.setUserPhone(this, phone.getText().toString());

            login();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void login() {

        final String url = Config.API_USER_BASE_URL + "login/";
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();

        System.out.println(jsonBody.toString());


        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Veuillez patienter ...");
        mProgress.show();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                jsonBody, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                System.out.println(" RESPONSE " + response.toString());

                try {
                    String status = response.getString("success");


                    if (status.equals("false")) {
                        //Une erreur est survenue
                        mProgress.dismiss();

                        DialogUtils.showConnectivityErrorDialog(ConnectionActivity.this, R.drawable.add_photo
                                , getString(R.string.connection_error), getString(R.string.connection_error_));

                    } else {

                        mProgress.dismiss();
                        //Enregistrement OK
                        startActivity(new Intent(ConnectionActivity.this, PhoneValidationActivity.class));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(" error " + error.toString());
                mProgress.dismiss();
                DialogUtils.showConnectivityErrorDialog(ConnectionActivity.this, R.drawable.add_photo
                        , getString(R.string.no_connection), getString(R.string.no_connection_long));
                error.printStackTrace();


            }
        }) {


        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        System.out.println("CommonUtils.startOnceTrackingService()");
        CommonUtils.startOnceTrackingService();

    }
}