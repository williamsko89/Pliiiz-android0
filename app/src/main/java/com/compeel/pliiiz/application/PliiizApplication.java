package com.compeel.pliiiz.application;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.service.MQTTService;
import com.compeel.pliiiz.service.RouteService;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class PliiizApplication extends Application {

    private static Context context;

    private static PliiizApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
        instance = this;

        // The default Realm file is "default.realm" in Context.getFilesDir();
        // we'll change it to "myrealm.realm"
        initRealm();
        startMQTTService();
        startTrackingService();

    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static Realm getRealmInstance() {
        // Get a Realm instance for this thread
        Realm realm = Realm.getDefaultInstance();

        return realm;
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("pliiiz.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public void startMQTTService() {
        System.out.println("startMQTTService");
        Intent serviceIntent = new Intent(this, MQTTService.class);
        startService(serviceIntent);
        System.out.println("startMQTTService end");
    }

    public void startTrackingService() {
        System.out.println("startTrackingService");
        Intent i = new Intent(this, RouteService.class);
        Bundle b = new Bundle();
        if (AccountUtils.getUserId(context).length() == 0)
            b.putString("locate", Config.ACTION_NO_TRACKING);
        else {
            if (AccountUtils.isPliiizer(context))
                b.putString("locate", Config.ACTION_START_TRACKING);
            else
                b.putString("locate", Config.ACTION_START_STOP_TRACKING);
        }

        i.setAction(CommonUtils.LOCATION_TRACKING_ACTION);
        i.putExtras(b);
        startService(i);
        System.out.println("startTrackingService end");
    }

    public static Context getContext() {
        return context;
    }
}