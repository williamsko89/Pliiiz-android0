package com.compeel.pliiiz;


import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.widget.CircularImageView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PliiiEndDetailActivity extends MainActivity implements
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback {

    private GoogleMap mMap;
    private Pliii mPliii;
    private Toolbar toolbar;

    private TextView pliiizer_name;
    private CircularImageView profile_image;
    private TextView pliiizer_note_information, pliii_invoice, pliii_total_price,title;
    private Button button_redo_pliii;


    private String mPliiizer_name;
    private String mPliiizer_lon;
    private String mPliiizer_lat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_detail_pliii_end);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pliiizer_name = (TextView) findViewById(R.id.pliiizer_name);
        profile_image = (CircularImageView) findViewById(R.id.profile_image);
        pliiizer_note_information = (TextView) findViewById(R.id.pliiizer_note_information);
        pliii_invoice = (TextView) findViewById(R.id.pliii_invoice);
        pliii_total_price = (TextView) findViewById(R.id.pliii_total_price);
        title = (TextView) findViewById(R.id.title);
        button_redo_pliii = (Button)findViewById(R.id.button_redo_pliii);

        setPliiiInfo();


    }

    private void getExtrasArguments() {

        String pliii_id = getIntent().getStringExtra("pliii_id");
        mPliii = DataBaseHelper.getPliiiInfoByID(pliii_id);
        mPliiizer_name = mPliii.getPliiizer_name();
        mPliiizer_lon = mPliii.getPliiizer_lon();
        mPliiizer_lat = mPliii.getPliiizer_lat();

    }

    private void setPliiiInfo() {
        getExtrasArguments();
        pliiizer_name.setText(mPliiizer_name);
        pliiizer_note_information.setText(getString(R.string.pliiizer_note_information) + " " + mPliiizer_name);
        pliii_invoice.setText(getPliiiInvoice(mPliii.getPliii_invoice()));
        pliii_total_price.setText(Html.fromHtml("<b>" + "TOTAL A PAYER : " + mPliii.getPliii_total_amount() + "</b>") + "\n");
        title.setText("COURSE "+mPliii.getRequest());


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        CommonUtils.loadImage(mPliii.getPliiizer_avatar(),profile_image);


        button_redo_pliii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //FinshRun API


                //Update pliii status in database
                startPliii(mPliii.getRequest());
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (mPliii != null) {

            Point displaySize = new Point();
            getWindowManager().getDefaultDisplay().getSize(displaySize);
            //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 200, 15));


        }


    }

    private String getPliiiInvoice(String invoice) {
        String line = "";
        try {
            JSONArray jsonArray = new JSONArray(invoice);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String price = jsonObject.getString("price");
                String product = jsonObject.getString("product");
                line += product + " : " + price + "\n";
            }
            line += "\n";
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
        catch (Exception a){
            return "";
        }
        return line;
    }


}
