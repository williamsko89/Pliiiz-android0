package com.compeel.pliiiz.holder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.compeel.pliiiz.R;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.widget.CircularImageView;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.Date;

public class PliiiHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView request, pliiizer, status;
    private RelativeTimeTextView date;
    private ImageView img_status;
    private RelativeLayout pliii_item;
    private AppCompatImageView pliiizers;
    private AppCompatRatingBar rating;



    private Context context;

    //itemView est la vue correspondante à 1 cellule
    public PliiiHolder(Context context, View itemView) {
        super(itemView);

        this.context = context;
        //c'est ici que l'on fait nos findView

        request = (TextView) itemView.findViewById(R.id.request);
        pliiizer = (TextView) itemView.findViewById(R.id.pliiizer);
        date = (RelativeTimeTextView) itemView.findViewById(R.id.date);

        status = (TextView) itemView.findViewById(R.id.status);
        img_status = (AppCompatImageView) itemView.findViewById(R.id.status_img);
        pliiizers = (AppCompatImageView) itemView.findViewById(R.id.pliiizers);
        rating = (AppCompatRatingBar) itemView.findViewById(R.id.rating);

        pliii_item = (RelativeLayout) itemView.findViewById(R.id.pliii_item);

    }

    //puis ajouter une fonction pour remplir la cellule en fonction d'un MyObject
    public void bind(Pliii pliii) {

        System.out.println("PLIIII ID "+pliii.getPliii_id());
        request.setText(pliii.getRequest());

        if(pliii.getRequest().toLowerCase().startsWith("http"))
            request.setText(context.getString(R.string.is_audio));
        pliiizer.setText(pliii.getPliiizer_name());

        if (AccountUtils.isPliiizer(context)) {
            pliiizer.setText(pliii.getCustomer_name());
        }


        if (pliii.getDate() == null)
            date.setReferenceTime(new Date().getTime());
        else
            date.setReferenceTime(Long.parseLong(pliii.getDate()));

        if (pliii.getStatus() == 0) {
            status.setText(R.string.en_cours);
            img_status.setImageResource(R.mipmap.location);
            pliii_item.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryLoght));

        } else if (pliii.getStatus() == 1) {
            status.setText(R.string.termine);
            img_status.setImageResource(R.drawable.success_gray);
            pliii_item.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
        } else if (pliii.getStatus() == 2) {
            status.setText(R.string.termine);
            img_status.setImageResource(R.drawable.success_gray);
            pliii_item.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
        }


        if (AccountUtils.isPliiizer(context)) {

            CommonUtils.loadImage(pliii.getCustomer_avatar(),pliiizers);

        } else {
            CommonUtils.loadImage(pliii.getPliiizer_avatar(),pliiizers);
        }

        rating.setMax(pliii.getRating());
    }

    @Override
    public void onClick(View v) {

    }
}