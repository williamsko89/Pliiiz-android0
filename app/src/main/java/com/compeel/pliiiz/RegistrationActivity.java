package com.compeel.pliiiz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.model.User;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.DialogUtils;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;


public class RegistrationActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION_PERMISSION = 1600;


    private Button button_register;
    private final static int PICK_PHOTO_FOR_AVATAR = 101;
    private ImageView profil;
    private CountryCodePicker ccp;

    private JSONObject jsonBody;
    private ProgressDialog mProgress;

    private EditText name, phone;
    private User user;


    @Override
    public void onStart() {

        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onStop() {

        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);


        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_LOCATION_PERMISSION);


        setContentView(R.layout.activity_registration);
        user = new User();
        jsonBody = new JSONObject();


        loadContentView();

    }

    private void loadContentView() {
        profil = (ImageView) findViewById(R.id.profile_image);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pick Image
                pickImage();
            }
        });


        name = (EditText) findViewById(R.id.nom);
        phone = (EditText) findViewById(R.id.phone);

        name.requestFocus();

        button_register = (Button) findViewById(R.id.button_register);
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidSignUpForm(name, phone))
                    registerUser();
                else
                    ;
            }
        });

        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.setCountryForNameCode(CommonUtils.getCountryCode(this));


    }


    private void registerUser() {

        mProgress = new ProgressDialog(RegistrationActivity.this);
        mProgress.setMessage("Registration ...");
        mProgress.show();


        try {

            jsonBody.put("name", name.getText().toString());
            jsonBody.put("phone", phone.getText().toString());
            jsonBody.put("address", "");
            jsonBody.put("uuid", CommonUtils.getDeviceID(this));
            jsonBody.put("lon", AccountUtils.getUserLon(this));
            jsonBody.put("lat", AccountUtils.getUserLat(this));


            user.setAddress("");
            user.setName(name.getText().toString());
            user.setFirst_name("");
            user.setLast_name("");
            user.setPhone(phone.getText().toString());
            user.setLast_name("");
            user.setDeviceID(CommonUtils.getDeviceID(this));

            createAccount();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                InputStream inputStream = RegistrationActivity.this.getContentResolver().openInputStream(data.getData());
                Bitmap image = BitmapFactory.decodeStream(inputStream);

                profil.setImageBitmap(image);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }
    }

    private boolean isValidSignUpForm(EditText signUpName,
                                      EditText signUpPhone) {
        boolean valid = true;
        View focus = null;
        String name = signUpName.getText().toString();
        String phone = ccp.getSelectedCountryCode() + signUpPhone.getText().toString();

        if (TextUtils.isEmpty(name)) {
            valid = false;
            signUpName.setError(getString(R.string.required_name));
            focus = signUpName;
        }

        if (TextUtils.isEmpty(phone)) {
            valid = false;
            signUpPhone.setError(getString(R.string.required_phone));
            focus = signUpPhone;
        }


        if (!valid) {
            focus.requestFocus();
        }

        return valid;
    }

    private void createAccount() {


        final String url = Config.API_USER_BASE_URL + "register";
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();

        System.out.println(jsonBody.toString());


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                jsonBody, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                System.out.println(" RESPONSE " + response.toString());

                try {
                    String status = response.getString("success");


                    if (status.equals("false")) {
                        //Une erreur est survenue
                        mProgress.dismiss();

                    } else {
                        //Enregistrement OK
                        //On enregistre les informations de l'utilisateur
                        AccountUtils.setUserInfo(user);

                        //Firt run OK
                        AccountUtils.setPrefUserFirstRun(RegistrationActivity.this, false);
                        //Lancement Page Phone validation
                        startActivity(new Intent(RegistrationActivity.this, PhoneValidationActivity.class));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(" error " + error.toString());


                mProgress.dismiss();
                DialogUtils.showConnectivityErrorDialog(RegistrationActivity.this, R.drawable.add_photo
                        , getString(R.string.no_connection), getString(R.string.no_connection_long));
            }
        }) {


        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                CommonUtils.startOnceTrackingService();

                break;
        }
    }
}


