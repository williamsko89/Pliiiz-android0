package com.compeel.pliiiz.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.compeel.pliiiz.R;
import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.DialogUtils;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;

public class RouteService extends Service {
    private static final String TAG = RouteService.class.getSimpleName();


    private LocationTracker tracker;
    private String LOCATE_ACTION;

    @Override
    public void onCreate() {
        super.onCreate();

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.v(TAG, "onStartCommand() " + intent);

        if (intent != null) {
            String action = intent.getAction();
            System.out.println("action" + LOCATE_ACTION);
            if (action != null) {
                if (action.equals(CommonUtils.LOCATION_TRACKING_ACTION)) {
                    Bundle b = intent.getExtras();

                    LOCATE_ACTION = b.getString("locate");
                    System.out.println("locate" + LOCATE_ACTION);
                    if (LOCATE_ACTION.equals("1")) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        }
                        startTracking();
                        try {
                            tracker.startListening();
                        } catch (Exception a) {

                        }

                    }
                    if (LOCATE_ACTION.equals("0")) {

                    }

                    if (LOCATE_ACTION.equals("-1")) {
                        //Get customer location
                        System.out.println("LOCATE_ACTION GRRR" + LOCATE_ACTION);
                        startTracking();
                        try {
                            tracker.startListening();
                        } catch (Exception a) {

                        }
                    }
                    if (LOCATE_ACTION.equals("-2")) {
                        stopSelf();
                    }
                }
            }

        }
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void startTracking() {
        TrackerSettings settings =
                new TrackerSettings()
                        .setUseGPS(true)
                        .setUseNetwork(true)
                        .setUsePassive(true)
                        .setTimeBetweenUpdates(1 * 60);

        // .setMetersBetweenUpdates(10);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        tracker = new LocationTracker(getApplicationContext(), settings) {
            @Override
            public void onLocationFound(Location location) {
                System.out.println("onLocationFound GRRR" + LOCATE_ACTION);
                if (LOCATE_ACTION.equals("-1")) {

                    System.out.println("onLocationFound LOCATE_ACTION -1");
                    //Register user location
                    AccountUtils.setUserLon(getApplicationContext(), String.valueOf(location.getLongitude()));
                    AccountUtils.setUserLat(getApplicationContext(), String.valueOf(location.getLatitude()));

                    tracker.stopListening();
                    System.out.println("onLocationFound tracker.stopListening(");
                }

                if (AccountUtils.isPliiizer(getApplicationContext())) {

                    System.out.println("onLocationFound getLatitude" + location.getLatitude());
                    System.out.println("onLocationFound getLongitude" + location.getLongitude());

                    //Register user location
                    AccountUtils.setUserLon(getApplicationContext(), String.valueOf(location.getLongitude()));
                    AccountUtils.setUserLat(getApplicationContext(), String.valueOf(location.getLatitude()));


                    //Send user location to the back end by excuting Background service
                    try{
                        System.out.println("onLocationFound getLongitude" + location.getLongitude());
                        set_pliiizer_current_location();
                    }
                    catch (Exception e){

                    }

                }


            }

            @Override
            public void onTimeout() {

            }
        };
        tracker.startListening();

    }


    private void set_pliiizer_current_location() {
        String pliii_id = AccountUtils.getCurrentPliii(PliiizApplication.getContext());
        final String url = Config.BASE_URL + "set_pliiizer_current_location/?format=json&pliii_id=" + pliii_id
                + "&pliiizer_id=" + AccountUtils.getUserId(PliiizApplication.getContext())
                + "&lon=" + AccountUtils.getUserLon(PliiizApplication.getContext())
                + "&lat=" + AccountUtils.getUserLat(PliiizApplication.getContext());

        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();


        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        System.out.println(response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogUtils.showConnectivityErrorDialog(PliiizApplication.getContext(), R.drawable.add_photo
                        , getString(R.string.no_connection), getString(R.string.no_connection_long));
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

}