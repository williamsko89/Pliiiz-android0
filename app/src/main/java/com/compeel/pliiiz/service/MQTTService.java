package com.compeel.pliiiz.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.ConversationWrapper;

import org.eclipse.paho.client.mqttv3.IMqttAsyncClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MQTTService extends Service {

    private static final String TAG = "MQTTService";
    private static boolean hasWifi = false;
    private static boolean hasMmobile = false;
    private Thread thread;
    private ConnectivityManager mConnMan;
    private volatile IMqttAsyncClient mqttClient;
    private String deviceId;

    private Looper mServiceLooper;
    public ServiceHandler mServiceHandler;
    private IMqttToken token;


    IBinder mIBinder = new PliiizBinder();
    ConversationWrapper conversationWrapper;


    public class PliiizBinder extends Binder {

        public MQTTService getMqttServiceInstance() {
            return MQTTService.this;
        }

    }

    class MQTTBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean hasConnectivity = false;
            boolean hasChanged = false;
            NetworkInfo infos[] = mConnMan.getAllNetworkInfo();

            for (int i = 0; i < infos.length; i++) {
                if (infos[i].getTypeName().equalsIgnoreCase("MOBILE")) {
                    if ((infos[i].isConnected() != hasMmobile)) {
                        hasChanged = true;
                        hasMmobile = infos[i].isConnected();
                    }
                    Log.d(TAG, infos[i].getTypeName() + " is " + infos[i].isConnected());
                } else if (infos[i].getTypeName().equalsIgnoreCase("WIFI")) {
                    if ((infos[i].isConnected() != hasWifi)) {
                        hasChanged = true;
                        hasWifi = infos[i].isConnected();
                    }
                    Log.d(TAG, infos[i].getTypeName() + " is " + infos[i].isConnected());
                }
            }

            hasConnectivity = hasMmobile || hasWifi;
            Log.v(TAG, "hasConn: " + hasConnectivity + " hasChange: " + hasChanged + " - " + (mqttClient == null || !mqttClient.isConnected()));
            if (hasConnectivity && hasChanged && (mqttClient == null || !mqttClient.isConnected())) {
                doConnect();
            } else if (!hasConnectivity && mqttClient != null && mqttClient.isConnected()) {
                Log.d(TAG, "doDisconnect()");
                try {
                    token = mqttClient.disconnect();
                    token.waitForCompletion(1000);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void onCreate() {

        //Avoid CPU Blocking
        HandlerThread thread = new HandlerThread(getPackageName(), Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);

        IntentFilter intentf = new IntentFilter();
        intentf.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(new MQTTBroadcastReceiver(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        mConnMan = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        conversationWrapper = new ConversationWrapper(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged()");
        android.os.Debug.waitForDebugger();
        super.onConfigurationChanged(newConfig);

    }


    public void subscribeTo(String channel) {

        try {
            token = mqttClient.subscribe(channel, 2);
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (Exception e) {

            try {
                mqttClient.disconnectForcibly();
                mqttClient.close();
                doConnect();
            } catch (MqttException e1) {
                e1.printStackTrace();
            }

        }
    }

    public void publishTextOnChannel(String channel, String msg) {

        final MqttMessage message = new MqttMessage(String.valueOf(msg).getBytes());
        try {
            token = mqttClient.publish(channel, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void doConnect() {
        Log.d(TAG, "doConnect()");

        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(false);
        options.setKeepAliveInterval(60);
        options.setAutomaticReconnect(true);


        try {
            deviceId = CommonUtils.getDeviceID(this);
            mqttClient = new MqttAsyncClient("tcp://" + Config.MQTT_BASE_URL + ":1883", deviceId, new MemoryPersistence());

            token = mqttClient.connect(options);


            mqttClient.setCallback(new MqttEventCallback());
            System.out.println("startMQTTService doConnect");

        } catch (MqttSecurityException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            switch (e.getReasonCode()) {
                case MqttException.REASON_CODE_BROKER_UNAVAILABLE:
                case MqttException.REASON_CODE_CLIENT_TIMEOUT:
                case MqttException.REASON_CODE_CONNECTION_LOST:
                case MqttException.REASON_CODE_SERVER_CONNECT_ERROR:
                    Log.v(TAG, "c" + e.getMessage());
                    e.printStackTrace();
                    break;
                case MqttException.REASON_CODE_FAILED_AUTHENTICATION:
                    Intent i = new Intent("RAISEALLARM");
                    i.putExtra("ALLARM", e);
                    Log.e(TAG, "b" + e.getMessage());
                    break;
                default:
                    Log.e(TAG, "a" + e.getMessage());
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "onStartCommand()");
        System.out.println("startMQTTService onStartCommand");

        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(CommonUtils.MQTT_PUBLISH_ACTION)) {
                    System.out.println("startMQTTService MQTT_PUBLISH_ACTION");
                    Bundle b = intent.getExtras();

                    String channel = b.getString("channel");
                    String message = b.getString("message");

                    System.out.println("startMQTTService MQTT_PUBLISH_ACTION " + channel);

                    publishTextOnChannel(channel, message);
                }
            }

        }

        return START_STICKY;
    }

    private class MqttEventCallback implements MqttCallback {


        @Override
        public void connectionLost(Throwable arg0) {

            Log.i(TAG, "connectionLost" + arg0.getMessage());
            try {
                mqttClient.disconnectForcibly();
                mqttClient.close();
            } catch (MqttException e) {
                e.printStackTrace();
            }

            doConnect();
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken arg0) {

        }

        @Override
        @SuppressLint("NewApi")
        public void messageArrived(String topic, final MqttMessage msg) throws Exception {

            System.out.println(" Unknown MESSAGE " + topic + " " + new String(msg.getPayload()));

            if (!topic.equals(AccountUtils.getUserId(PliiizApplication.getContext())))
                System.out.println(" Unknonw MESSAGE " + new String(msg.getPayload()));

            else {
                Log.i(TAG, "Message arrived from topic" + topic);

                Handler h = new Handler(getMainLooper());
                h.post(new Runnable() {
                    @Override
                    public void run() {

                        //Pass message to Conversation Wrapper
                        conversationWrapper.buildConversationMesage(new String(msg.getPayload()));
                        System.out.println(" Incoming " + new String(msg.getPayload()));
                    }
                });
            }

        }
    }


    public String getThread() {
        return Long.valueOf(thread.getId()).toString();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind called");
        return mIBinder;
    }


    private final class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mqttClient.disconnect();
            mqttClient.close();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}