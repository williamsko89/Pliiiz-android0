package com.compeel.pliiiz.model;


import java.io.Serializable;

import io.realm.PliiiRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

// Define Pliii model class by extending RealmObject
public class Pliii extends RealmObject {

    @PrimaryKey
    public long id;

    private String request;
    private String pliiizer_name;
    private String pliiizer_lat, pliiizer_lon;
    private String pliiizer_avatar;
    private String pliiizer_id;
    private String pliii_id;
    private String customer_lat;
    private int rating = 0;

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {

        return rating;

    }

    public String getCustomer_lat() {
        return customer_lat;
    }

    public void setCustomer_lat(String customer_lat) {
        this.customer_lat = customer_lat;
    }

    public String getCustomer_lon() {
        return customer_lon;
    }

    public void setCustomer_lon(String customer_lon) {
        this.customer_lon = customer_lon;
    }

    private String customer_lon;

    private String pliii_amount;
    private String pliii_total_amount;
    private boolean pliiizer_arrived;

    public boolean isPliiizer_accept() {
        return pliiizer_accept;
    }

    public void setPliiizer_accept(boolean pliiizer_accept) {
        this.pliiizer_accept = pliiizer_accept;
    }

    private boolean pliiizer_accept;

    public boolean isPliii_confirmed() {
        return pliii_confirmed;
    }

    public void setPliii_confirmed(boolean pliii_confirmed) {
        this.pliii_confirmed = pliii_confirmed;
    }

    private boolean pliii_confirmed;

    private String pliii_invoice;

    private String customer_name, customer_avatar, customer_id;

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_avatar() {
        return customer_avatar;
    }

    public void setCustomer_avatar(String customer_avatar) {
        this.customer_avatar = customer_avatar;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getPliii_invoice() {
        return pliii_invoice;
    }

    public void setPliii_invoice(String pliii_invoice) {
        this.pliii_invoice = pliii_invoice;
    }

    public boolean isPliiizer_arrived() {
        return pliiizer_arrived;
    }

    public void setPliiizer_arrived(boolean pliiizer_arrived) {
        this.pliiizer_arrived = pliiizer_arrived;
    }

    public String getPliii_amount() {
        return (pliii_amount == null) ? "0" : pliii_amount;
    }

    public void setPliii_amount(String pliii_amount) {
        this.pliii_amount = pliii_amount;
    }

    public String getPliii_total_amount() {
        return (pliii_total_amount == null) ? "0" : pliii_total_amount;
    }

    public void setPliii_total_amount(String pliii_total_amount) {
        this.pliii_total_amount = pliii_total_amount;
    }

    public String getPliii_id() {
        return pliii_id;
    }

    public void setPliii_id(String pliii_id) {
        this.pliii_id = pliii_id;
    }

    public String getPliiizer_id() {
        return pliiizer_id;
    }

    public void setPliiizer_id(String pliiizer_id) {
        this.pliiizer_id = pliiizer_id;
    }

    public String getPliiizer_avatar() {
        return pliiizer_avatar;
    }

    public void setPliiizer_avatar(String pliiizer_avatar) {
        this.pliiizer_avatar = pliiizer_avatar;
    }

    private String date;
    private int status;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getPliiizer_name() {
        return pliiizer_name;
    }

    public void setPliiizer_name(String pliiizer_name) {
        this.pliiizer_name = pliiizer_name;
    }

    public String getPliiizer_lat() {
        return pliiizer_lat;
    }

    public void setPliiizer_lat(String pliiizer_lat) {
        this.pliiizer_lat = pliiizer_lat;
    }

    public String getPliiizer_lon() {
        return pliiizer_lon;
    }

    public void setPliiizer_lon(String pliiizer_lon) {
        this.pliiizer_lon = pliiizer_lon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Pliii(String request, String pliiizer_name, String pliiizer_lat, String pliiizer_lon, String date, int status) {

        this.request = request;
        this.pliiizer_name = pliiizer_name;
        this.pliiizer_lat = pliiizer_lat;
        this.pliiizer_lon = pliiizer_lon;
        this.date = date;
        this.status = status;
    }

    public Pliii() {

    }
    // ... Generated getters and setters ...
}
