package com.compeel.pliiiz.model;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by william .
 */
public class Conversation extends RealmObject {

    public static final int IN = 0;
    public static final int OUT = 1;

    public static final int INVOICE = 1;
    public static final int AUDIO = 2;
    public static final int TEXT = 3;
    public static final int RCV_NEW_PLIII = 4;
    public static final int RCV_CONFIRMED_INVOICE = 5;
    public static final int RCV_NEW_PLIII_AUDIO = 6;


    private int type;
    private int direction;
    private String content;
    private String date;
    private Pliii pliii;

    public Pliii getPliii() {
        return pliii;
    }

    public void setPliii(Pliii pliii) {
        this.pliii = pliii;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @PrimaryKey @Index
    public long id;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Conversation() {

    }

    public Conversation(int type, int direction, String content, String date) {

        this.type = type;

        this.direction = direction;
        this.content = content;
        this.date = date;
    }
}
