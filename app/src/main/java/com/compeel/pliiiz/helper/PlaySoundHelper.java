package com.compeel.pliiiz.helper;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.widget.SeekBar;

import com.compeel.pliiiz.R;
import com.compeel.pliiiz.utilities.CommonUtils;

/**
 * Created by William on 21/09/2017.
 */

public class PlaySoundHelper implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener {

    Context context;
    String url;
    SeekBar seekBar;
    AppCompatImageView media_control;
    Handler mHandler = new Handler();

    public PlaySoundHelper(Context context, String path, SeekBar seekBar, AppCompatImageView media_control) {
        this.context = context;
        this.url = path;
        this.seekBar = seekBar;
        this.media_control = media_control;

        this.seekBar.setProgress(0);
        this.seekBar.setOnSeekBarChangeListener(this);

        //create player
        player = new MediaPlayer();
        initMusicPlayer();
    }

    void initMusicPlayer() {


        //set player properties

        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //set listeners
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    //media player
    private MediaPlayer player;

    @Override
    public void onCompletion(MediaPlayer mp) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        mp.reset();
        mp.stop();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mp.reset();
        mp.stop();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {

        //start playback
        mp.start();
        //set the data source
        seekBar.setProgress(0);
        seekBar.setMax(100);


    }

    //play a song
    public void play() {
        //play//get song

        if(player.isPlaying()){
            player.pause();

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                media_control.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.audio_play));
            } else {
                media_control.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.audio_play));
            }

        }
        else {

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                media_control.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.audio_pause));
            } else {
                media_control.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.audio_pause));
            }

            try {

                player.setDataSource(url);
            } catch (Exception e) {
                Log.e("MUSIC SERVICE", "Error setting data source", e);
            }
            System.out.println("URL " + url);
            player.prepareAsync();
        }

    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = player.getDuration();
            long currentDuration = player.getCurrentPosition();

            // Updating progress bar
            int progress = (int) (CommonUtils.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);
            seekBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    /**
     *
     * */
    @Override
    public void onProgressChanged(SeekBar seeBar, int progress, boolean fromTouch) {
        seekBar.setProgress(progress);
    }

    /**
     * When user starts moving the progress handler
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = player.getDuration();
        int currentPosition = CommonUtils.progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        player.seekTo(currentPosition);

        // update timer progress again
        updateProgressBar();
    }

    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

}
