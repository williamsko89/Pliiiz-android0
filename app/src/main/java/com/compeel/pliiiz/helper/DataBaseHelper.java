package com.compeel.pliiiz.helper;

import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.model.Conversation;
import com.compeel.pliiiz.model.Pliii;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by William on 13/09/2017.
 */

public class DataBaseHelper {

    public static ArrayList getPliiiList(){

        Realm realm = PliiizApplication.getRealmInstance();
        RealmResults pliiis = realm.where(Pliii.class).findAll();
        ArrayList pliiisArray = new ArrayList(pliiis);

        return pliiisArray;
    }

    public static ArrayList getEndedPliiiList(){

        Realm realm = PliiizApplication.getRealmInstance();
        RealmResults pliiis = realm.where(Pliii.class).equalTo("status",1).findAll();
        ArrayList pliiisArray = new ArrayList(pliiis);

        return pliiisArray;
    }

    public static ArrayList getRunningPliiiList(){

        Realm realm = PliiizApplication.getRealmInstance();
        RealmResults pliiis = realm.where(Pliii.class).equalTo("status",0).findAll();
        ArrayList pliiisArray = new ArrayList(pliiis);

        return pliiisArray;
    }

    public static ArrayList getConversationListByPliiiID(String pliii_id){

        Realm realm = PliiizApplication.getRealmInstance();
        RealmResults conversations = realm.where(Conversation.class).equalTo("pliii.pliii_id",pliii_id).findAll();
        ArrayList conversationsArrayList = new ArrayList(conversations);

        return conversationsArrayList;
    }



    public static int getPliiiAutoIncrementValue(){
        Realm realm = PliiizApplication.getRealmInstance();
        Number num = realm.where(Pliii.class).max("id");
        int nextID;
        if(num == null) {
            nextID = 1;
        } else {
            nextID = num.intValue() + 1;
        }
        return nextID;
    }

    public static int getConversationAutoIncrementValue(){
        Realm realm = PliiizApplication.getRealmInstance();
        Number num = realm.where(Conversation.class).max("id");
        int nextID;
        if(num == null) {
            nextID = 1;
        } else {
            nextID = num.intValue() + 1;
        }
        return nextID;
    }

    public static Pliii getPliiiInfoByID(String pliii_id){
        // Create the Realm instance
        Realm realm = PliiizApplication.getRealmInstance();
        Pliii pliii = realm.where(Pliii.class).equalTo("pliii_id",pliii_id).findFirst(); //add in this line isNotNull()

        return  pliii;
    }

    public static void updatePliiiStatus(String pliii_id,int status){
        Realm realm = PliiizApplication.getRealmInstance();
        Pliii pliii = realm.where(Pliii.class).equalTo("pliii_id",pliii_id).findFirst(); //add in this line isNotNull()
        realm.beginTransaction();
        pliii.setStatus(status);
        realm.commitTransaction();
    }


    public static void acceptPliii(String pliii_id){
        Realm realm = PliiizApplication.getRealmInstance();
        Pliii pliii = realm.where(Pliii.class).equalTo("pliii_id",pliii_id).findFirst(); //add in this line isNotNull()
        realm.beginTransaction();
        pliii.setPliiizer_accept(true);
        realm.commitTransaction();
    }
    public static void confirmPliii(String pliii_id){
        Realm realm = PliiizApplication.getRealmInstance();
        Pliii pliii = realm.where(Pliii.class).equalTo("pliii_id",pliii_id).findFirst(); //add in this line isNotNull()
        realm.beginTransaction();
        pliii.setPliii_confirmed(true);
        realm.commitTransaction();
    }


    public static void setPliiiRate(String pliii_id,int rate){
        Realm realm = PliiizApplication.getRealmInstance();
        Pliii pliii = realm.where(Pliii.class).equalTo("pliii_id",pliii_id).findFirst(); //add in this line isNotNull()
        realm.beginTransaction();
        pliii.setRating(rate);
        realm.commitTransaction();
    }
}
