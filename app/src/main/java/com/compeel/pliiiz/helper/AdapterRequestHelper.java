package com.compeel.pliiiz.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.compeel.pliiiz.MainActivity;
import com.compeel.pliiiz.PliiizerPliiiListActivity;
import com.compeel.pliiiz.R;
import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.model.Conversation;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

/**
 * Created by William on 17/09/2017.
 */

public class AdapterRequestHelper {

    private static ProgressDialog mProgress;

    public static void acceptPliii(final Context context ,final  Conversation _conversation){
        DataBaseHelper.acceptPliii(_conversation.getPliii().getPliii_id());
        mProgress = new ProgressDialog(context);
        mProgress.setMessage("Veuillez patienter ...");
        mProgress.show();


        final String url = Config.BASE_URL + "accept_run_pliiizer/?pliii_id="+_conversation.getPliii().getPliii_id()+"&accept=1";
        RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();


        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        System.out.println(" RESPONSE " + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("success");


                            if (status.equals("false")) {
                                //Une erreur est survenue
                                mProgress.dismiss();
                                Toast.makeText(context, context.getString(R.string.no_result_found), Toast.LENGTH_LONG).show();


                            } else {
                                //On ne fait rien on attend réponse MQTT
                                mProgress.dismiss();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogUtils.showConnectivityErrorDialog(context,R.drawable.add_photo
                        ,context.getString(R.string.no_connection),context.getString(R.string.no_connection_long));

                error.printStackTrace();


                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pliii_id", _conversation.getPliii().getPliii_id());
                params.put("accept", "1");

                return params;
            }


        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);

    }


    public static void confirmPliii(final Context context ,final  Conversation _conversation){
        DataBaseHelper.acceptPliii(_conversation.getPliii().getPliii_id());
        mProgress = new ProgressDialog(context);
        mProgress.setMessage("Veuillez patienter ...");
        mProgress.show();


        final String url = Config.BASE_URL + "confirm_run_customer/?pliii_id="+_conversation.getPliii().getPliii_id()+"&confirm=1";
        RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();


        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        System.out.println(" RESPONSE " + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("success");


                            if (status.equals("false")) {
                                //Une erreur est survenue
                                mProgress.dismiss();
                                Toast.makeText(context, context.getString(R.string.no_result_found), Toast.LENGTH_LONG).show();


                            } else {
                                //On ne fait rien on attend réponse MQTT
                                mProgress.dismiss();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(" error " + error.toString());
                DialogUtils.showConnectivityErrorDialog(context,R.drawable.add_photo
                        ,context.getString(R.string.no_connection),context.getString(R.string.no_connection_long));

                error.printStackTrace();


                mProgress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pliii_id", _conversation.getPliii().getPliii_id());
                params.put("confirm", "1");

                return params;
            }


        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);

    }
}
