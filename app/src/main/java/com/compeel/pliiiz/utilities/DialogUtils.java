package com.compeel.pliiiz.utilities;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import com.compeel.pliiiz.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by William on 29/09/2017.
 */

public class DialogUtils {

    public static void showConnectivityErrorDialog(Context context,int img,String title,String body) {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setCustomImage(img)
                .setContentText(body)
                .show();
    }

    public static void showInfoDialog(Context context,int img,String title,String body) {
        new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setCustomImage(img)
                .setContentText(body)
                .show();
    }
}
