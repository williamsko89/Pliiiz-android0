package com.compeel.pliiiz.utilities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.telephony.TelephonyManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.compeel.pliiiz.R;
import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.service.RouteService;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by William on 11/09/2017.
 */

public class CommonUtils {

    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static String file_exts[] = {AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP};
    public static int currentFormat = 0;


    public static String START_MQTT_SERVICE_ACTION = "START_MQTT_SERVICE_ACTION";
    public static String MQTT_PUBLISH_ACTION = "com.compeel.action.PUBLISH";
    public static String LOCATION_TRACKING_ACTION = "com.compeel.action.start_location_tracking";
    public static final int CREATE_PLIII_INVOICE_REQUEST = 100;

    public static String getCountryCode(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getSimCountryIso();
        return countryCode;
    }

    public static String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getFileAbsolutePathForFile(String filename) {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" + filename);
    }


    public static String getFileName() {
        return (System.currentTimeMillis() + file_exts[currentFormat]);
    }


    public static void deleteFile(String FilePath) {
        File file = new File(FilePath, AUDIO_RECORDER_FOLDER);

        if (file.exists())
            file.delete();
    }


    /**
     * Function to change progress to timer
     *
     * @param progress      -
     * @param totalDuration returns current duration in milliseconds
     */
    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }

    /**
     * Function to get Progress percentage
     *
     * @param currentDuration
     * @param totalDuration
     */
    public static int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage

        return percentage.intValue();
    }


    public static boolean isValidEmailAddress(String email) {
        java.util.regex.Pattern p = java.util.regex.Pattern
                .compile(".+@.+\\.[a-z]+");
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static boolean isValidPhoneNumber(String phone) {
        Pattern validPhonePattern = Pattern.compile("(\\+|00)(?:[0-9] ?){6,14}[0-9]$");
        Matcher m = validPhonePattern.matcher(phone);
        return m.matches();
    }

    public static void playSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(PliiizApplication.getContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void loadImage(String url, final AppCompatImageView imgView) {
        Glide.with(PliiizApplication.getContext()).load(url).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {


                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imgView.setBackgroundDrawable(ContextCompat.getDrawable(PliiizApplication.getContext(), R.drawable.default_bg));
                } else {
                    imgView.setBackgroundDrawable(ContextCompat.getDrawable(PliiizApplication.getContext(), R.drawable.default_bg));
                }

                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(imgView)

        ;

    }


    public static void stopTrackingService() {
        Intent i = new Intent(PliiizApplication.getContext(), RouteService.class);
        Bundle b = new Bundle();
        b.putString("locate", "-2");
        i.setAction(CommonUtils.LOCATION_TRACKING_ACTION);
        i.putExtras(b);
        PliiizApplication.getContext().startService(i);
    }


    public static void startIndefiniteTrackingService() {
        Intent i = new Intent(PliiizApplication.getContext(), RouteService.class);
        Bundle b = new Bundle();
        b.putString("locate", "1");
        i.setAction(CommonUtils.LOCATION_TRACKING_ACTION);
        i.putExtras(b);
        PliiizApplication.getContext().startService(i);
    }

    public static void startOnceTrackingService() {
        Intent i = new Intent(PliiizApplication.getContext(), RouteService.class);
        Bundle b = new Bundle();
        b.putString("locate", "-1");
        i.setAction(CommonUtils.LOCATION_TRACKING_ACTION);
        i.putExtras(b);
        PliiizApplication.getContext().startService(i);
    }
}
