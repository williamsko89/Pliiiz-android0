package com.compeel.pliiiz.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.text.TextUtils;


import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.model.User;

import java.io.IOException;
import java.net.URL;


/**
 * Created by William on 10/07/2017.
 */
public class AccountUtils {


    public static final String PREF_ACCOUNT_NAME = "account_name";
    public static final String PREF_USER_FIRST_NAME = "first_name";
    public static final String PREF_USER_LAST_NAME = "last_name";
    public static final String PREF_AUTH_TOKEN = "device_id";
    public static final String PREF_USER_MAIL = "mail";
    public static final String PREF_USER_ADDRESS = "address";
    public static final String PREF_USER_PHONE = "phone";
    public static final String PREF_USER_FIRST_RUN = "first_run";
    public static final String PREF_USER_ID = "id";
    public static final String PREF_USER_LON = "user_lon";
    public static final String PREF_USER_LAT = "user_lat";

    public static final String PREF_USER_IS_PLIIIZER = "is_pliiizer";
    public static final String PREF_USER_PLIIIZER_CURRENT_PLIII = "current_pliii";

    public static void setUserInfo(User user) {
        if (user != null) {

            if (!TextUtils.isEmpty(user.getName()))
                setAccountName(PliiizApplication.getContext(), user.getName());

            if (!TextUtils.isEmpty(user.getMail()))
                setUserMail(PliiizApplication.getContext(), user.getMail());

            if (!TextUtils.isEmpty(user.getPhone()))
                setUserPhone(PliiizApplication.getContext(), user.getPhone());

            if (!TextUtils.isEmpty(user.getAddress()))
                setUserAddress(PliiizApplication.getContext(), user.getAddress());

            if (!TextUtils.isEmpty(user.getCurrentLon()))
                setUserLon(PliiizApplication.getContext(), user.getCurrentLon());

            if (!TextUtils.isEmpty(user.getCurrentLat()))
                setUserLat(PliiizApplication.getContext(), user.getCurrentLat());

            if (!TextUtils.isEmpty(user.getUserID()))
                setUserID(PliiizApplication.getContext(), user.getUserID());


            //setUserId(CitydiaApplication.getContext(), user.id);

        }
    }

    public static boolean isAuthenticated(final Context context) {
        return !TextUtils.isEmpty(getAccountName(context));
    }

    public static String getCurrentPliii(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_PLIIIZER_CURRENT_PLIII, null);
    }

    public static String getAccountName(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_ACCOUNT_NAME, null);
    }



    public static String getPhone(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_PHONE, "");
    }

    public static String getAdress(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_ADDRESS, "");
    }


    public static boolean isFirstRun(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_USER_FIRST_RUN, true);
    }

    public static String getUserLon(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_LON, "");
    }

    public static String getUserLat(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_LAT, "");
    }

    public static boolean isPliiizer(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_USER_IS_PLIIIZER, false);
    }

    /**
     * Get the logged in user id.
     *
     * @param context
     * @return The current user id. Return -1 if the preference does not exist.
     * <p>
     */
    public static String getUserId(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_USER_ID, "");
    }


    private static void setAuthToken(final Context context, final String authToken) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_AUTH_TOKEN, authToken).apply();
    }

    public static void setUserPhone(final Context context, String phone) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_USER_PHONE, phone).apply();
    }

    public static void setUserAddress(Context context, String address) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_USER_ADDRESS, address).apply();
    }

    public static void setUserMail(Context context, String mail) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_USER_MAIL, mail).apply();
    }


    public static void setPrefUserFirstRun(Context context, boolean firstRun) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putBoolean(PREF_USER_FIRST_RUN, firstRun).apply();
    }

    public static void setUserProfile(Context context, boolean firstRun) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putBoolean(PREF_USER_IS_PLIIIZER, firstRun).apply();
    }

    /**
     * Set the user unique account name which serve to identify uniquely a user(usually user mail)
     *
     * @param context
     * @param accountname
     */
    private static void setAccountName(final Context context, final String accountname) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_ACCOUNT_NAME, accountname).apply();
    }


    public static void setUserLon(Context context, String lon) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_USER_LON, lon).apply();
    }

    public static void setUserLat(Context context, String lat) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_USER_LAT, lat).apply();
    }

    public static void setUserID(Context context, String id) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_USER_ID, id).apply();
    }

    public static void setCurrentPliii(Context context, String pliii_id) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(PREF_USER_PLIIIZER_CURRENT_PLIII, pliii_id).apply();
    }




}
