package com.compeel.pliiiz.utilities;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.compeel.pliiiz.MainActivity;
import com.compeel.pliiiz.PliiiActivity;
import com.compeel.pliiiz.PliiizerArrivedDetailActivity;
import com.compeel.pliiiz.R;
import com.compeel.pliiiz.application.PliiizApplication;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.model.Conversation;
import com.compeel.pliiiz.model.Pliii;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.realm.Realm;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by William on 13/09/2017.
 */

public class ConversationWrapper {

    Context context;


    public ConversationWrapper(Context context) {
        this.context = context;
    }

    public void buildConversationMesage(String entryMessage) {
        CommonUtils.playSound();
        try {


            System.out.println("ENTRYMESSAGE " + new JSONObject(entryMessage).toString());
            JSONObject jsonObject = new JSONObject(entryMessage);
            String msgType = jsonObject.getString("msg_type");



            //Handle answer sent by pliiizer after receiving a new pliii request
            if (msgType.equals("2")) {



                //Pre message : build conversation message
                String pliii_id = jsonObject.getString("pliii_id");
                String request = jsonObject.getString("request");
                String pre_msg = jsonObject.getString("pre_msg");

                //Retreive pliiizer information
                String pliiizer_id = jsonObject.getString("pliiizer_id");
                String pliiizer_name = jsonObject.getString("pliiizer_name");
                String pliiizer_lon = jsonObject.getString("pliiizer_lon");
                String pliiizer_lat = jsonObject.getString("pliiizer_lat");
                String pliiizer_avatar = jsonObject.getString("pliiizer_avatar");

                //Retreive user information
                String customer_id = jsonObject.getString("customer_id");
                String customer_name = jsonObject.getString("customer_name");
                String customer_avatar = jsonObject.getString("customer_avatar");

                //Create a pliii
                //Record pliii in database
                Realm realm = PliiizApplication.getRealmInstance();
                realm.beginTransaction();

                Pliii pliii = realm.createObject(Pliii.class, DataBaseHelper.getPliiiAutoIncrementValue());
                pliii.setPliiizer_lat(pliiizer_lat);
                pliii.setPliiizer_lon(pliiizer_lon);
                pliii.setPliiizer_name(pliiizer_name);
                pliii.setPliiizer_avatar(pliiizer_avatar);
                pliii.setPliiizer_id(pliiizer_id);
                pliii.setPliii_id(pliii_id);
                pliii.setStatus(0);
                pliii.setRequest(request);
                pliii.setDate("" + new Date().getTime());
                pliii.setCustomer_avatar(customer_avatar);
                pliii.setCustomer_name(customer_name);
                pliii.setCustomer_id(customer_id);

                realm.commitTransaction();

                //Record first conversation
                realm = PliiizApplication.getRealmInstance();
                realm.beginTransaction();

                Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
                conversation.setDate("" + new Date().getTime());
                conversation.setType(Conversation.TEXT);
                conversation.setDirection(Conversation.IN);
                conversation.setContent(pre_msg);
                conversation.setPliii(pliii);
                realm.commitTransaction();


                //Send information to mainActivity
                if (MainActivity.mProgress.isShowing() || MainActivity.mProgress != null)
                    MainActivity.mProgress.dismiss();


                //Launch Activity
                launchPliiiActivity(entryMessage);
            }


            //Handle pliii request received by pliiizer
            if (msgType.equals("0")) {


                //Pre message : build conversation message
                String pliii_id = jsonObject.getString("pliii_id");
                String request = jsonObject.getString("request");

                String is_audio = jsonObject.getString("is_audio");

                //Retreive pliiizer information
                String pliiizer_id = jsonObject.getString("pliiizer_id");
                String pliiizer_name = jsonObject.getString("pliiizer_name");
                String pliiizer_lon = jsonObject.getString("pliiizer_lon");
                String pliiizer_lat = jsonObject.getString("pliiizer_lat");
                String pliiizer_avatar = jsonObject.getString("pliiizer_avatar");

                //Retreive user information
                String customer_id = jsonObject.getString("customer_id");
                String customer_name = jsonObject.getString("customer_name");
                String customer_avatar = jsonObject.getString("customer_avatar");
                String customer_lon = jsonObject.getString("customer_lon");
                String customer_lat = jsonObject.getString("customer_lat");

                //Create a pliii
                //Record pliii in database
                Realm realm = PliiizApplication.getRealmInstance();
                realm.beginTransaction();

                Pliii pliii = realm.createObject(Pliii.class, DataBaseHelper.getPliiiAutoIncrementValue());
                pliii.setPliiizer_lat(pliiizer_lat);
                pliii.setPliiizer_lon(pliiizer_lon);
                pliii.setPliiizer_name(pliiizer_name);
                pliii.setPliiizer_avatar(pliiizer_avatar);
                pliii.setPliiizer_id(pliiizer_id);
                pliii.setPliii_id(pliii_id);
                pliii.setCustomer_lat(customer_lat);
                pliii.setCustomer_lon(customer_lon);
                pliii.setStatus(0);
                pliii.setRequest(request);
                if (is_audio.toLowerCase().equals("true")) {
                    pliii.setRequest(jsonObject.getString("audio"));
                }
                pliii.setDate("" + new Date().getTime());
                pliii.setCustomer_avatar(customer_avatar);
                pliii.setCustomer_name(customer_name);
                pliii.setCustomer_id(customer_id);

                realm.commitTransaction();

                //Record first conversation
                realm.beginTransaction();

                String pre_msg = context.getString(R.string.accept_msg1) + " " + pliiizer_name + " " + context.getString(R.string.accept_msg2);

                Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
                conversation.setDate("" + new Date().getTime());

                if (is_audio.toLowerCase().equals("true")) {
                    conversation.setType(Conversation.RCV_NEW_PLIII_AUDIO);

                } else
                    conversation.setType(Conversation.RCV_NEW_PLIII);
                conversation.setDirection(Conversation.IN);
                conversation.setContent(pre_msg);
                conversation.setPliii(pliii);
                realm.commitTransaction();

                AccountUtils.setCurrentPliii(context, pliii_id);

                //Launch Activity
                launchPliiiActivity(entryMessage);


            }


            //Pliii invoice
            if (msgType.equals("4")) {

                //get Pliii info here
                String pliii_id = jsonObject.getString("pliii_id");
                String invoice = jsonObject.getString("invoice");
                String total_amount = jsonObject.getString("total_amount");

                Pliii pliii = DataBaseHelper.getPliiiInfoByID(pliii_id);

                Realm realm = PliiizApplication.getRealmInstance();

                realm.beginTransaction();
                pliii.setPliii_total_amount(total_amount);
                pliii.setPliii_invoice(invoice);
                realm.commitTransaction();

                //Create new invoice conversation

                realm.beginTransaction();

                Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
                conversation.setDate("" + new Date().getTime());
                conversation.setType(Conversation.INVOICE);
                conversation.setDirection(Conversation.IN);
                conversation.setContent(invoice);
                conversation.setPliii(pliii);
                realm.commitTransaction();


                //Launch Activity
                launchPliiiActivity(entryMessage);
            }

            //Pliiizer arrived
            if (msgType.equals("7")) {
                String pliii_id = jsonObject.getString("pliii_id");
                String lon = jsonObject.getString("pliiizer_lon");
                String lat = jsonObject.getString("pliiizer_lat");

                Pliii pliii = DataBaseHelper.getPliiiInfoByID(pliii_id);

                Realm realm = PliiizApplication.getRealmInstance();
                realm.beginTransaction();
                pliii.setPliiizer_arrived(true);
                pliii.setPliiizer_lat(lat);
                pliii.setPliiizer_lon(lon);
                realm.commitTransaction();

                launchPliiiDetailActivity(pliii_id);
                pliiizerArrivedNotofication(pliii_id, pliii.getPliiizer_name());
            }

            //Normal Text arrived
            if (msgType.equals("9")) {

                //{"msg_type":"10","from":"612287","to":"gYQKtiIH","message":"oui oui",
                //        "created_at":9499494949,"pliiiz_id":"980098428","to_name":"hello world hello world",
                //        "from_name":"William de SOUZA"}


                System.out.println(jsonObject.toString());


                String pliii_id = jsonObject.getString("pliii_id");
                String message = jsonObject.getString("message");
                String from_name = jsonObject.getString("from_name");

                System.out.println(pliii_id);
                System.out.println(message);


                Pliii pliii = DataBaseHelper.getPliiiInfoByID(pliii_id);


                Realm realm = PliiizApplication.getRealmInstance();
                realm.beginTransaction();
                Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
                conversation.setDate("" + new Date().getTime());
                conversation.setType(Conversation.TEXT);
                conversation.setDirection(Conversation.IN);
                conversation.setContent(message);
                conversation.setPliii(pliii);
                realm.commitTransaction();

                if (AccountUtils.getCurrentPliii(context).equals(pliii_id)) {
                    //Create notification here
                    launchPliiiActivity(entryMessage);
                } else
                    pushChatNotification(jsonObject.toString(), message, from_name);


            }


            //Handle pliii invoice confirmed
            if (msgType.equals("6")) {

                //Pre message : build conversation message
                String pliii_id = jsonObject.getString("pliii_id");

                //Create a pliii
                //Record pliii in database
                Realm realm = PliiizApplication.getRealmInstance();
                Pliii pliii = DataBaseHelper.getPliiiInfoByID(pliii_id);

                //Record first conversation
                realm = PliiizApplication.getRealmInstance();
                realm.beginTransaction();

                Conversation conversation = realm.createObject(Conversation.class, DataBaseHelper.getConversationAutoIncrementValue());
                conversation.setDate("" + new Date().getTime());
                conversation.setType(Conversation.RCV_CONFIRMED_INVOICE);
                conversation.setDirection(Conversation.IN);
                conversation.setContent("");
                conversation.setPliii(pliii);
                realm.commitTransaction();

                //Launch Activity
                launchPliiiActivity(entryMessage);


            }


            //Pliii endend
            if (msgType.equals("8")) {

                //get Pliii info here
                String pliii_id = jsonObject.getString("pliii_id");
                String rating = jsonObject.getString("rating");


                //Finish Pliii
                Pliii pliii = DataBaseHelper.getPliiiInfoByID(pliii_id);
                Realm realm = PliiizApplication.getRealmInstance();
                realm.beginTransaction();
                pliii.setStatus(1);
                pliii.setRating(Integer.parseInt(rating));
                realm.commitTransaction();

                //Set current plii to None
                AccountUtils.setCurrentPliii(context, "");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void launchPliiiActivity(String entryMessage) {
        Intent launchA = new Intent(context, PliiiActivity.class);
        Bundle bundle = new Bundle();
        System.out.println("kkk " + entryMessage);
        bundle.putString("message", entryMessage);
        launchA.putExtras(bundle);
        //TODO write somethinkg that has some sense
        if (Build.VERSION.SDK_INT >= 11) {
            launchA.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        } else {
            launchA.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(launchA);

    }

    private void launchPliiiDetailActivity(String pliii_id) {

        //Launch here DetailPliiiActivity
        Bundle b = new Bundle();
        b.putString("pliii_id", pliii_id);
        Intent intent = new Intent(this.context, PliiizerArrivedDetailActivity.class);
        intent.putExtras(b);

        if (Build.VERSION.SDK_INT >= 11) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void pliiizerArrivedNotofication(String pliii_id, String from) {
        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        Intent notificationIntent = new Intent(context, PliiiActivity.class);

        Bundle b = new Bundle();
        b.putString("pliii_id", pliii_id);
        Intent intent = new Intent(this.context, PliiizerArrivedDetailActivity.class);
        intent.putExtras(b);
        //TODO write somethinkg that has some sense
        if (Build.VERSION.SDK_INT >= 11) {
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        } else {
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);


        //set
        builder.setContentIntent(contentIntent);
        builder.setSmallIcon(R.drawable.pliiiz_logo_orange);
        builder.setContentText("Le pliiizer " + from + "est là");
        builder.setContentTitle("Pliiiz");
        builder.setAutoCancel(true);
        builder.setDefaults(Notification.DEFAULT_ALL);

        Notification notification = builder.build();
        nm.notify((int) System.currentTimeMillis(), notification);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void pushChatNotification(String entryMessage, String message, String from) {
        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        Intent notificationIntent = new Intent(context, PliiiActivity.class);

        Bundle bundle = new Bundle();
        bundle.putString("message", entryMessage);
        notificationIntent.putExtras(bundle);

        //TODO write somethinkg that has some sense
        if (Build.VERSION.SDK_INT >= 11) {
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        } else {
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        //set
        builder.setContentIntent(contentIntent);
        builder.setSmallIcon(R.mipmap.icon_pliiiz);
        builder.setContentText(message);
        builder.setContentTitle(from);
        builder.setAutoCancel(true);
        builder.setDefaults(Notification.DEFAULT_SOUND);

        Notification notification = builder.build();
        nm.notify((int) System.currentTimeMillis(), notification);
    }
}
