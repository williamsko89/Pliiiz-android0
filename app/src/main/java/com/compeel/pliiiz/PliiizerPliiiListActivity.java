package com.compeel.pliiiz;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.compeel.pliiiz.adapter.PliiiAdapter;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.service.MQTTService;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.DialogUtils;

import java.util.ArrayList;


public class PliiizerPliiiListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Toolbar toolbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView listView;
    private CheckBox pliiizer_status;

    PliiiAdapter pliiiAdapter;
    ArrayList pliiis;

    public MQTTService mqttService;
    private boolean bounded;
    private boolean is_available;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //patient = (Patient)getIntent().getSerializableExtra("patient");

        setContentView(R.layout.activity_list_pliiizer_pliii);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        pliiizer_status = (CheckBox) findViewById(R.id.pliiizer_status);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);

        listView = (RecyclerView) findViewById(R.id.list_course);
        listView.setLayoutManager(new LinearLayoutManager(this));


        loadAllPliii();
        change_pliiizer_status();


    }

    private void change_pliiizer_status() {
        pliiizer_status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    is_available = true;

                    sendStatus("0");
                } else {
                    is_available = false;

                    sendStatus("1");
                }
            }
        });
    }


    private void loadAllPliii() {

        pliiis = DataBaseHelper.getPliiiList();
        pliiiAdapter = new PliiiAdapter(this, pliiis);
        pliiiAdapter.notifyDataSetChanged();
        listView.setAdapter(pliiiAdapter);


    }

    private void sendStatus(String status) {
        final String url = Config.BASE_URL + "change_pliiizer_status/?format=json&pliiizer_id=" + AccountUtils.getUserId(this) + "&is_available=" + status;
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();


        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        pliiizer_status.setChecked(is_available);


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                DialogUtils.showConnectivityErrorDialog(PliiizerPliiiListActivity.this, R.drawable.add_photo
                        , getString(R.string.no_connection), getString(R.string.no_connection_long));
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    private void refreshList() {
        //do processing to get new data and set your listview's adapter, maybe  reinitialise the loaders you may be using or so
        //when your data has finished loading, cset the refresh state of the view to false
        loadAllPliii();
        AccountUtils.setCurrentPliii(this, "");
        swipeRefreshLayout.setRefreshing(false);

    }


    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bounded = true;
            MQTTService.PliiizBinder pliiizBinder = (MQTTService.PliiizBinder) service;
            mqttService = pliiizBinder.getMqttServiceInstance();

            Toast.makeText(PliiizerPliiiListActivity.this, "Connexion OK", Toast.LENGTH_SHORT).show();

            mqttService.subscribeTo(AccountUtils.getUserId(PliiizerPliiiListActivity.this));


        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bounded = false;
            mqttService = null;
        }
    };


    @Override
    protected void onStart() {
        super.onStart();

        Intent mIntent = new Intent(this, MQTTService.class);
        bindService(mIntent, mServiceConnection, BIND_AUTO_CREATE);

        CommonUtils.startIndefiniteTrackingService();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bounded) {
            unbindService(mServiceConnection);
        }
        CommonUtils.stopTrackingService();

    }


    @Override
    protected void onResume() {
        super.onResume();
        refreshList();
        CommonUtils.startIndefiniteTrackingService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        CommonUtils.startIndefiniteTrackingService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CommonUtils.startIndefiniteTrackingService();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CommonUtils.stopTrackingService();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        CommonUtils.startIndefiniteTrackingService();
    }
}
