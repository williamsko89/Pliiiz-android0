package com.compeel.pliiiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.compeel.pliiiz.fragment.ConversationFragment;
import com.compeel.pliiiz.fragment.PliiiRouteFragment;
import com.compeel.pliiiz.helper.DataBaseHelper;
import com.compeel.pliiiz.model.Pliii;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PliiiActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private TextView pliiizer_name;
    private RelativeTimeTextView pliii_hour;
    private AppCompatImageView pliii_invoice, pliii_chat, pliiizer_location;
    private AppCompatImageView pliiizer_avatar;

    public ViewPager viewPager;

    private List<Fragment> mFragmentList;
    private List<String> mFragmentTitleList;

    private String mPliiizer_name;
    private String mPliii_date;
    private String mPliiizer_avatar;
    private String mPliii_id;
    private Intent mPliii;

    private String customer_name, customer_avatar;

    private JSONObject incomingMsg;
    private Pliii pliii;
    private boolean openChat = true;

    public static boolean activity_state = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        activity_state = true;
        System.out.println("onCreate");
        setContentView(R.layout.activity_pliii);

        mFragmentList = new ArrayList<>();
        mFragmentTitleList = new ArrayList<>();

        getIncomingMessage();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(2);
        setupViewPager(viewPager);


        pliiizer_name = (TextView) findViewById(R.id.user_name);
        pliii_hour = (RelativeTimeTextView) findViewById(R.id.pliii_hour);
        pliii_invoice = (AppCompatImageView) findViewById(R.id.pliii_add_invoice);
        pliiizer_avatar = (AppCompatImageView) findViewById(R.id.user_profile);

        pliii_chat = (AppCompatImageView) findViewById(R.id.pliii_chat);
        pliiizer_location = (AppCompatImageView) findViewById(R.id.pliiizer_location);

        pliii_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
                pliii_chat.setImageResource(R.drawable.chat_on);
                pliiizer_location.setImageResource(R.drawable.location);

            }
        });

        pliiizer_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
                pliii_chat.setImageResource(R.drawable.chat_off);
                pliiizer_location.setImageResource(R.drawable.location_primary);
            }
        });

        //Show pliii_add_invoice only when user is pliiizer
        if (!AccountUtils.isPliiizer(this)) {
            pliii_invoice.setVisibility(View.GONE);
        }
        setPliiiInfo();
        pliii_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPliiiInvoice();
            }
        });


        viewPager.setCurrentItem(0);
        pliii_chat.setImageResource(R.drawable.chat_on);
        pliiizer_location.setImageResource(R.drawable.location);

    }

    private void getIncomingMessage() {
        Intent intent = getIntent();
        try {
            if (intent.hasExtra("message")) {
                incomingMsg = new JSONObject(intent.getStringExtra("message"));

                mPliii_id = incomingMsg.getString("pliii_id");
                openChat = true;


            } else if (intent.hasExtra("pliii_id")) {
                mPliii_id = intent.getStringExtra("pliii_id");
                openChat = false;

            } else {
                getExtrasArguments();
            }

            //get pliii object
            pliii = DataBaseHelper.getPliiiInfoByID(mPliii_id);
            System.out.println("incomingMsg 2 " + pliii);

            getPliiiInfo();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void getPliiiInfo() {

        mPliiizer_name = pliii.getPliiizer_name();
        mPliii_date = pliii.getDate();
        mPliii_id = pliii.getPliii_id();
        mPliiizer_avatar = pliii.getPliiizer_avatar();

        if (AccountUtils.isPliiizer(this)) {
            customer_name = pliii.getCustomer_name();
            customer_avatar = pliii.getCustomer_avatar();
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


        adapter.addFrag(new ConversationFragment().newInstance(mPliii_id), "");
        adapter.addFrag(new PliiiRouteFragment().newInstance(mPliii_id), "");

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                System.out.println("position " + position);

                if (position == 0) {
                    pliii_chat.setImageResource(R.drawable.chat_on);
                    pliiizer_location.setImageResource(R.drawable.location);

                }
                if (position == 1) {
                    pliii_chat.setImageResource(R.drawable.chat_off);
                    pliiizer_location.setImageResource(R.drawable.location_primary);

                }




            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {

            if (object instanceof ConversationFragment) {
                ConversationFragment f = (ConversationFragment) object;
                if (f != null) {
                    f.update();
                }
            }

            return super.getItemPosition(object);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return mFragmentTitleList.get(position);

        }
    }

    private void getExtrasArguments() {

        mPliii = getIntent();
        mPliiizer_name = mPliii.getStringExtra("pliiizer_name");
        mPliii_date = mPliii.getStringExtra("pliii_date");
        mPliii_id = mPliii.getStringExtra("pliii_id");
        mPliiizer_avatar = mPliii.getStringExtra("pliiizer_avatar");

        //Si c'est un pliiizer, on récupère les infos user
        if (AccountUtils.isPliiizer(this)) {
            customer_name = mPliii.getStringExtra("customer_name");
            customer_avatar = mPliii.getStringExtra("customer_avatar");
        }


    }


    private void setPliiiInfo() {

        AccountUtils.setCurrentPliii(this, pliii.getPliii_id());

        pliiizer_name.setText(mPliiizer_name);
        if (AccountUtils.isPliiizer(this)) {
            pliiizer_name.setText(customer_name);
        }

        if (mPliii_date == (null))
            pliii_hour.setReferenceTime(new Date().getTime());
        else
            pliii_hour.setReferenceTime(Long.parseLong(mPliii_date));

        if (AccountUtils.isPliiizer(this)) {
            CommonUtils.loadImage(customer_avatar, pliiizer_avatar);

        } else

            CommonUtils.loadImage(mPliiizer_avatar, pliiizer_avatar);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getIncomingMessage();
        updateConversationFragment();

    }

    private void addPliiiInvoice() {

        Intent i = new Intent(this, InvoiceActivity.class);
        i.putExtra("pliii_id", pliii.getPliii_id());
        if (pliii.getRequest().startsWith("http")) {
            i.putExtra("request", getString(R.string.is_audio));
        } else
            i.putExtra("request", pliii.getRequest());

        startActivityForResult(i, CommonUtils.CREATE_PLIII_INVOICE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CommonUtils.CREATE_PLIII_INVOICE_REQUEST:
                    //Récupération résultat ici
                    String invoice = data.getExtras().getString("invoice");
                    if (invoice.length() == 0) {
                    } else {


                        updateConversationFragment();

                    }

                    System.out.println("invoice " + invoice);
                    break;
                default:
                    break;
            }
        }

    }

    public interface Updateable {
        public void update();
    }

    private void updateConversationFragment() {
        //Update conversation fragment
        viewPager.getAdapter().notifyDataSetChanged();
        viewPager.setCurrentItem(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume");
        activity_state = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Changer état activité en Pause
        System.out.println("onPause");
        activity_state = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("onStart");
        activity_state = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("onStop");
        activity_state = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("onDestroy");
        activity_state = false;
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        System.out.println("onDetachedFromWindow");
        activity_state = false;
    }
}
