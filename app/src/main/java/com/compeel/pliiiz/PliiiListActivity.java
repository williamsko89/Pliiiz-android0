package com.compeel.pliiiz;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import com.compeel.pliiiz.fragment.AllPliiiFragment;
import com.compeel.pliiiz.fragment.EndPliiiFragment;
import com.compeel.pliiiz.fragment.RunningPliiiFragment;

import java.util.ArrayList;
import java.util.List;


public class PliiiListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private List<Fragment> mFragmentList;
    private List<String> mFragmentTitleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //patient = (Patient)getIntent().getSerializableExtra("patient");

        setContentView(R.layout.activity_list_pliii);
        mFragmentList = new ArrayList<>();
        mFragmentTitleList = new ArrayList<>();


        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        createTabIcons();
    }


    private void createTabIcons() {
        tabLayout.getTabAt(0).setText(getString(R.string.tout));
        tabLayout.getTabAt(1).setText(getString(R.string.en_cours));
        tabLayout.getTabAt(2).setText(getString(R.string.termine));

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new AllPliiiFragment(), getString(R.string.tout));
        adapter.addFrag(new RunningPliiiFragment(), getString(R.string.en_cours));
        adapter.addFrag(new EndPliiiFragment(), getString(R.string.termine));
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    //Return current fragment on basis of Position
    public Fragment getFragment(int pos) {
        return mFragmentList.get(pos);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return mFragmentTitleList.get(position);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
