package com.compeel.pliiiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.compeel.pliiiz.config.Config;
import com.compeel.pliiiz.http.VolleySingleton;
import com.compeel.pliiiz.model.User;
import com.compeel.pliiiz.service.RouteService;
import com.compeel.pliiiz.utilities.AccountUtils;
import com.compeel.pliiiz.utilities.CommonUtils;
import com.compeel.pliiiz.utilities.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;


public class PhoneValidationActivity extends AppCompatActivity {
    private static final int REQUEST_AUDIO_PERMISSION = 1500;


    private TextView register;
    private ProgressDialog mProgress;
    private PinEntryEditText pinEntry;
    private Button button_connect, no_rcv_code;
    private JSONObject jsonBody;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_validation);

        pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);
        pinEntry.requestFocus();
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {

                }
            });
        }
        register = (TextView) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PhoneValidationActivity.this, RegistrationActivity.class));
            }
        });

        button_connect = (Button) findViewById(R.id.button_connect);
        button_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBody();
            }
        });

        no_rcv_code = (Button) findViewById(R.id.no_rcv_code);
        no_rcv_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Resend the passcode
            }
        });

        requestAudioPermission();

    }


    private void getBody() {


        try {

            jsonBody = new JSONObject();
            jsonBody.put("passcode", pinEntry.getText().toString());
            jsonBody.put("phone", AccountUtils.getPhone(this));
            jsonBody.put("lon", Double.parseDouble(AccountUtils.getUserLon(this)));
            jsonBody.put("lat", Double.parseDouble(AccountUtils.getUserLat(this)));

            validatePhone();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void validatePhone() {

        final String url = Config.API_USER_BASE_URL + "phone_validation/";
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();

        System.out.println(jsonBody.toString());


        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Veuillez patienter pendant que Pliiiz vérifie votre numero de téléphone ...");
        mProgress.show();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                jsonBody, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                System.out.println(" RESPONSE " + response.toString());

                try {
                    String status = response.getString("success");


                    if (status.equals("false")  ) {
                        //Une erreur est survenue
                        mProgress.dismiss();
                        DialogUtils.showConnectivityErrorDialog(PhoneValidationActivity.this, R.drawable.add_photo
                                , getString(R.string.code_incorrect), getString(R.string.retry_code));

                    } else {

                        //Enregistrement Info user dans preference
                        JSONObject juser = response.getJSONObject("user");


                        User user = new User();
                        user.setAddress(juser.getString("address"));
                        user.setMail(juser.getString("email"));
                        user.setName(juser.getString("name"));
                        user.setFirst_name("");
                        user.setLast_name("");
                        user.setPhone(juser.getString("phone"));
                        user.setLast_name("");

                        user.setDeviceID(CommonUtils.getDeviceID(PhoneValidationActivity.this));

                        //Check if client is pliiizer
                        String is_pliiizer = response.getString("is_pliiizer");
                        if (is_pliiizer.equals("true")) {
                            System.out.println("conn " + is_pliiizer);
                            String pliiizer_id = response.getJSONObject("pliiizer").getString("id");
                            System.out.println("conn " + pliiizer_id);
                            user.setUserID(pliiizer_id);
                        } else {
                            user.setUserID(juser.getString("id"));
                        }


                        AccountUtils.setUserInfo(user);

                        //Firt run OK
                        AccountUtils.setPrefUserFirstRun(PhoneValidationActivity.this, false);

                        //Pliiizer or not
                        if (response.getString("is_pliiizer").equals("true")) {
                            AccountUtils.setUserProfile(PhoneValidationActivity.this, true);
                            Snackbar.make(pinEntry, getString(R.string.app_name), Snackbar.LENGTH_LONG).show();
                            startActivity(new Intent(PhoneValidationActivity.this, PliiizerPliiiListActivity.class));
                            //Pliii List
                        } else {
                            AccountUtils.setUserProfile(PhoneValidationActivity.this, false);
                            startActivity(new Intent(PhoneValidationActivity.this, MainActivity.class));
                        }
                        mProgress.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(" error " + error.toString());


                error.printStackTrace();
                mProgress.dismiss();
                DialogUtils.showConnectivityErrorDialog(PhoneValidationActivity.this, R.drawable.add_photo
                        , getString(R.string.no_connection), getString(R.string.no_connection_long));
                error.printStackTrace();
            }
        }) {
        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);
    }

    private void requestAudioPermission() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.RECORD_AUDIO},
                    REQUEST_AUDIO_PERMISSION);

            return;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }





    private void resendCode(){

        try {

            jsonBody = new JSONObject();
            jsonBody.put("phone", AccountUtils.getPhone(this));


            final String url = Config.API_USER_BASE_URL + "resend_passcode/";
            RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();

            System.out.println(jsonBody.toString());


            mProgress = new ProgressDialog(this);
            mProgress.setMessage(getString(R.string.wait));
            mProgress.show();

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                    jsonBody, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    System.out.println(" RESPONSE " + response.toString());

                    try {
                        String status = response.getString("success");


                        if (status.equals("false")  ) {
                            //Une erreur est survenue
                            mProgress.dismiss();
                            DialogUtils.showConnectivityErrorDialog(PhoneValidationActivity.this, R.drawable.add_photo
                                    , getString(R.string.code_incorrect), getString(R.string.retry_code));

                        } else {

                            mProgress.dismiss();
                            DialogUtils.showInfoDialog(PhoneValidationActivity.this, R.drawable.add_photo
                                    , getString(R.string.app_name), getString(R.string.no_result_found));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println(" error " + error.toString());


                    error.printStackTrace();
                    mProgress.dismiss();
                    DialogUtils.showConnectivityErrorDialog(PhoneValidationActivity.this, R.drawable.add_photo
                            , getString(R.string.no_connection), getString(R.string.no_connection_long));
                    error.printStackTrace();
                }
            }) {
            };

            req.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(req);



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
